var nw = require('nw.gui');
var fs = require('fs'); //File System
var http = require('http');//http server
var url = require('url');//URL-rewriting
var request = require('request');//Remote requests
var path = require('path');//local file system location checking (if files/folders exist)
var open = require('open');
var torrentStream = require('torrent-stream');//torrent-file streaming.

global.start_time = Date.now();//used for RNG-generation.

var Random = require('random-js');
var r = new Random(Random.engines.mt19937().seedWithArray([0x12345678, 0x90abcdef]));


var last_fm_api_key = "b04d2d2731e95ad8ceef4225a18a0bd1";
var LastFmNode = require('lastfm').LastFmNode;



var lastfm = new LastFmNode({
  api_key: last_fm_api_key,
  secret: "c5c716e05d443f54ab45da0fa91c1c6e"
});

global.server_location = 'http://cactusplayer.com.cp-in-6.webhostbox.net/test_server/';// 'http://www.cactusplayer.com';



global.no_cover_img = 'epictriangle_emptyalbum.png';

//global.music_server_portnum = 335;


//Plug in data in user interface:
//fs.readFile('./package.json', 'utf-8', function (error, contents) {
    //document.getElementById('packagedata').innerHTML = contents;
//});



var trackerslist = ["http://tracker.thepiratebay.org/announce",
    "udp://tracker.thepiratebay.org:80/announce",
    "http://tracker.publicbt.com/announce",
    "udp://tracker.publicbt.com:80/announce",
    "udp://tracker.openbittorrent.com:80/announce",
    "http://tracker.openbittorrent.com/announce",
    "http://tracker.deadfrog.us:42426/announce",
    "http://tracker.publicbt.com/announce",
    "udp://tracker.publicbt.com:80/announce",
    "udp://tracker.openbittorrent.com:80/announce",
    "http://tracker.openbittorrent.com/announce",
    "http://tracker.paradise-tracker.com:1...",
    "udp://tracker.openbittorrent.com:80",
    "http://tracker.torrenty.org:6969/anno...",
    "http://tracker.token.ro:80/announce",
    "http://tracker.publicbt.org:80/announce",
    "http://tracker.prq.to:80/announce",
    "http://tracker.publicbt.com:80/announce",
    "http://tracker.ydy.com:83/announce",
    "http://tracker.xpear.de:80/announce",
    "http://tracker.istole.it:80/announce",
    "http://tracker.ydy.com:97/announce",
    "http://tracker.harryy.us:80/announce",
    "http://photodiode.mine.nu:80/announce",
    "http://photodiode.mine.nu:6969/announce",
    "http://tracker.torrentparty.com:80/announce",
    "http://bt.peerseed.ru/announce",
    "http://tracker.ydy.com:103/announce",
    "http://tracker.openbittorrent.com:80/announce",
    "http://tracker.xpear.de:6969/announce",
    "http://denis.stalker.h3q.com:80/announce",
    "http://tracker.edgebooster.com:6969/announce",
    "http://pow7.com:80/announce",
    "http://tracker.publicbt.com:80/announce",
    "http://tracker.sladinki007.net:6500/a...",
    "http://tracker.xpear.de:6969/announce",
    "http://moviesb4time.biz/announce.php",
    "http://tracker.irc.su/announce",
    "http://tracker.prq.to/announce",
    "http://tracker.dunnsearch.org/announce.php",
    "http://tracker.irc.su:80/announce",
    "http://photodiode.mine.nu:6969/announce",
    "http://tracker.edgebooster.com:6969/announce",
    "http://tracker.bittorrent.am/announce",
    "http://denis.stalker.h3q.com:80/announce",
    "http://tpb.tracker.prq.to/announce",
    "http://tv.tracker.prq.to/announce",
    "http://tracker.bittorrent.am:80/announce",
    "http://tracker.prq.to/announce",
    "http://tpb.tracker.prq.to/announce",
    "http://www.ipmart-forum.com:2710/anno...",
    "http://tracker.ydy.com:105/announce",
    "http://www.torrentvideos.com:6969/ann...",
    "http://exodus.desync.com:6969/announce",
    "http://tracker.token.ro:80/announce",
    "http://tracker.publicbt.org:80/announce",
    "http://tracker.prq.to:80/announce",
    "http://tracker.publicbt.com:80/announce",
    "http://10.rarbg.com:80/announce",
    "http://tracker.xpear.de:80/announce",
    "http://tracker.istole.it:80/announce",
    "http://www.torrent-downloads.to:2710/...",
    "http://tracker.harryy.us:80/announce",
    "http://photodiode.mine.nu:80/announce",
    "http://photodiode.mine.nu:6969/announce",
    "http://tracker.torrentparty.com:80/announce",
    "http://bt.peerseed.ru/announce",
    "http://tracker.ydy.com:100/announce",
    "http://tracker.openbittorrent.com:80/announce"];



global.torrents = {};//Array of torrents that are indexed using torrent-stream.

global.played_since_start = 0;
global.played_since_last_support_modal = 0;
global.artists_to_support = [];


$(document).ready(function(){
    loadKnownHostsList();

    setInterval(updateKnownHosts,60000 * 60);//Once every hour
})


function updateKnownHosts(){
    //1. Ask current host for server list.
    request(global.server_location+'/json.php?list_servers', function(error, response, body){
        if(typeof(response)==='undefined'){
            console.log("Error: Could not connect to server at all. Internet is probably gone.");
            return;
        }
        if(response.statusCode != 200) {
            console.log("Error getting list_servers from current server.");
            return;
        }
        try{
            var new_server_list = JSON.parse(body);
        }catch(e){
            console.log("Error while parsing list_servers JSON from server:", e);
            return;
        }

        //2. Copy over all hosts from reply that I did not yet know about.
        var server_list = new_server_list.concat(global.known_hosts.list);
        server_list = eliminateDuplicates(server_list);
        console.log("New Server List is now:",server_list);

        //3. Save result in HostsList File and also update current working copy.
        global.known_hosts.list = server_list;
        saveKnownHostsList();
        
        //4. Update DOM element containing the hosts.
        showServersDomList();
    });

}
function showServersDomList(){
    var result = "<ul>";
    $.each(global.known_hosts.list, function(i,host){
        result += "<li data-host-index='"+i+"'";
        if(i==global.known_hosts.current){
            result += " class='current'";
        }
        result +=" ><span>"+host+"</span><i class='open_external glyphicon glyphicon-new-window'></i></li>";
    });
    result += "</ul>";
    $('.known_hosts_list').html(result);
    $('.known_hosts_list li').click(function(e){
        var host_index =  $(this).attr('data-host-index');
        changeCurrentServer(host_index);
        saveKnownHostsList();
    });

    $('.known_hosts_list li .open_external').click(function(e){
        var host_index =  $(this).parent().attr('data-host-index');
        var address = $.trim(global.known_hosts.list[host_index]);
        gui.Shell.openExternal(address);
        e.stopPropagation();
    });
}


/*
function eliminateDuplicateArtists(artists){
    var uniqueArtists = [];
    for(var i=0;i<artists.length;i++){
        containsDuplicate = false;
        for var j=i+1;j<artists.length;j++){
            if(artists[i]['name'] == artists[j]['name']){
                contains_duplicate = true;
                break;
            };
        }
        if(!containsDuplicate){
            uniqueArtists.push(artists[i]);
        }
    }
    return uniqueArtists;
}
*/
/*

function registerFiles(urn, fi) {
    if(fi!==undefined) {
        global.torrents[urn].files.forEach(function(file, index) {//Deselect all files, especially non-music files.
            //console.log('torrent filename:', file.name);
            //file.deselect();
            if(index==fi) {
                file.select();
                console.log('selecting file:'+ index +' -- '+file.name+' to play in the future!\n');
            }
        });
        
    }

}

function registerTorrent(urn, fi) {
    if (global.torrents[urn] !== undefined) {
        registerFiles(urn, fi);
        return true;
    }
    global.torrents[urn] = torrentStream('magnet:?xt=urn:btih:'+urn, {trackers: trackerslist});//torrentStream('magnet:?xt=urn:btih:535194236610322444a7cf381ddf865e6be854fe&dn=Beatles+-+Hard+Day%26%23039%3Bs+Night+%28full+Album%29+320&xl=36214490&dl=36214490');
    global.torrents[urn].isready = false;

    console.log('starting  connecting to torrent peers for '+urn);
    global.torrents[urn].on('ready', function() {
        console.log('finished connecting to torrent peers for '+urn);
        global.torrents[urn].isready = true;
        registerFiles(urn, fi);
    });

    global.torrents[urn].on('download', function(pieceindex) {
        if(typeof readStream !=='undefined') {
            console.log('downloaded:'+ (pieceindex - readStream.startPiece ) + ' of ' + (readStream.endPiece - readStream.startPiece)+' ('+urn+') '  );
            var file = getFileFromPiece(global.torrents[urn].torrent, pieceindex);
            file.buffered = pieceindex * global.torrents[urn].torrent.pieceLength / file.size;
            console.log("Buffer Overlay:",buffer_overlay);
            console.log("Buffer Overlay File:",file.buffered);
            
            var buffer_overlay = $('.buffer_overlay', '.playlistsong[data-urn='+urn+']');
            buffer_overlay.css('width', (file.buffered*100) +'%');
        }
    });



    return global.torrents[urn];
}



function setupMusicServer() {



    //Load .mp3 in back-end
    http.createServer(function(request, response) {
        console.log("MusicServer is visited!");

        // 1. Get file name 
        var url_parts = url.parse(request.url, true); 
        var GET_params = url_parts.query;
        if(GET_params.urn===undefined || GET_params.fi===undefined) {
            response.writeHead(404);
            //response.write(JSON.stringify(GET_params));
            response.end();
            return false;
        }



        if(global.torrents[GET_params.urn].isready) {
            response.setHeader("Access-Control-Allow-Origin","*");
            returnMusicInfo(request, response, GET_params.urn, GET_params.fi);
        }else{
            global.torrents[GET_params.urn].on('ready',function() {
                returnMusicInfo(request, response, GET_params.urn, GET_params.fi);
            });
        }
        
    })
    .listen(global.music_server_portnum);
}


function returnMusicInfo(request, response, urn, fi) {
    try{
        var finalfile = global.torrents[urn].files[fi];//Get the wanted file from engine.
    }catch(e){
        // 2. If it does not exist, return with error. 
        console.log(e);
        return false;
    }




    // 3. Otherwise, calculate content length and other headers 

    var fileLength = finalfile.length;
    var contentLen = fileLength;

    var httpheaders = {
        'Content-Type': 'audio/mpeg',
        'Accept-Ranges': 'bytes',
            'Content-Length': fileLength //stat.size
        };

        // 4. Return content-range to enable streaming before whole file is downloaded.
        var bytStr = 'bytes=';
        if(request.headers.range && request.headers.range.substr(0,bytStr.length) == bytStr) {
            httpstatuscode = 206;
            console.log('range '+request.headers.range);
            var bytSelection = request.headers.range.substr(bytStr.length);

            var bytDashPos = bytSelection.indexOf('-');
            var bytPreDash = bytSelection.substr(0, bytDashPos);
            var bytEndDash = bytSelection.substr(bytDashPos+1);
            var offset = 0;

            if(bytPreDash == '0') {
                if(bytEndDash) {
                    var reqRangeLen = parseInt(bytEndDash,10);
                    if(reqRangeLen > contentLen) {
                        console.log('accept asking for invalid range');
                        rangeString = bytPreDash + '-' + (contentLen - 1);
                    } else {
                        contentLen = reqRangeLen+1;
                        rangeString = bytPreDash + '-' + bytEndDash;
                    }
                } else {
                    rangeString = '0-' + (fileLength-1).toString();//gs.length
                }
            } else if(bytEndDash != '' && bytPreDash != '') {
                contentLen = (parseInt(bytEndDash,10)+1) - parseInt(bytPreDash,10);
                offset = parseInt(bytPreDash,10);
                rangeString = bytPreDash + '-' + bytEndDash;
            } else if(bytEndDash == '' && bytPreDash != '') {
            // ex, 1234-
            contentLen = contentLen - parseInt(bytPreDash,10);
            offset = parseInt(bytPreDash,10) - 1;
            rangeString = bytPreDash + '-' + (fileLength - 1).toString();//gs.length
        }
        httpheaders["Content-Range"] = 'bytes ' + rangeString+'/'+fileLength;
        console.log('resulting content range:'+ httpheaders["Content-Range"]);
        httpheaders["Content-Length"] = contentLen;

    }else{
        // console.log('Visited without range request');
        httpstatuscode = 200;
        /*response.write("ASDF");
        response.end('');
        return false;
        
    }
    // 4. If HEAD request, return file size as header field only.
    response.writeHead(httpstatuscode, httpheaders);
    if(request.method == 'HEAD') {
        console.log('doing a HEAD request');
        response.end('');
    }else{

        // 5. Otherwise, read wanted part of file/torrent stream, and return to client.
        chunkSize = contentLen;

        if(chunkSize > fileLength - offset) {
            chunkSize = fileLength - offset;
        }

        readStream = finalfile.createReadStream({start: offset , end: offset + chunkSize });//fs.createReadStream(filePath);
        // We replaced all the event handlers with a simple call to readStream.pipe()
        console.log(finalfile.name);
        if(typeof readStream ===undefined) {
            response.writeHead(404);
            response.end();
            return false;
        }else{
            readStream.pipe(response);
        }

    }
}
*/
function mSecToTime(msec){
    var sec = msec/1000;
    var min = Math.floor(sec/60);
    var restsec = Math.floor(sec%60);
    if(restsec<10){
        restsec = "0"+restsec;
    }
    return min + ":" + restsec;
}

player = {};
function playTrackFromTorrent(urn, fi) {
    console.log("Server is listening");


    //Play the sound from the port

    //1. Stop player that was playing previously.
    if(typeof(player)!=='undefined' && typeof(player.playing)!=='undefined' && player.playing){
        try{
            player.stop();
            //player.startPlaying();

        }catch(e){

        }
    }

    try{
        if(typeof global.playlist[global.PLAYLIST_INDEX] !== 'undefined' && typeof(global.playlist[global.PLAYLIST_INDEX].player)!=='undefined'){
            player = global.playlist[global.PLAYLIST_INDEX].player;//AV.Player.fromURL('http://localhost:333?urn='+urn+'&fi='+fi);
            //console.log("Current Player:", player);
            //player = new AV.Player(player.asset);//Reset the asset.

            $('.progressbuffered').css('width', player.buffered +'%');
            $('.progressmoving').css('width',   0 +'%');



        //interval = setInterval(function() {

            /*if(typeof(player)!=='undefined' && typeof(player.playing)!=='undefined' && player.playing){
                try{
                    player.stop();//Neccessary to reset Aurora player asset (decoding buffer).
                    
                }catch(e){

                }
            }
            player.play();
            try{
                //player.startPlaying();
            }catch(e){

            }*/
            player.volume = global.PLAYER_VOLUME;
            player.play();
            player.hasAlreadyPlayed = true;
            //console.log('attempting to play');

                
            




            player.on('buffer', function(percent) {
                //console.log('buffering:'+ percent);
                $('.progressbuffered').css('width', (percent) + '%');
                        //clearInterval(interval);
                    });

            player.off('progress');//Turn of earlier events for this.
            player.on('progress', function(currentTime) {
                        rewardTrackLoaded();
                        //var duration = estimatePlayDuration();
                        if(player.duration==0) { 
                            //var duration = estimatePlayDuration();
                            var duration = 0;
                        }else{
                            var duration = player.duration;
                        }


                        $('.progressmoving').css('width', (currentTime / duration) * 100+ '%');
                        //$('.progressmoving').css('width', percent + '%');
                        $('.player_duration').html(mSecToTime(player.duration));
                        $('.player_current_time').html(mSecToTime(player.currentTime));


                        //If is set, scrobble track after > half is played.
                        if(localStorage.CONFIG_enable_lastfm_scrobbling=="true"){
                            var artist = global.playlist[global.PLAYLIST_INDEX]['artist'];
                            var track = global.playlist[global.PLAYLIST_INDEX]['trackname'];
                            if(typeof(player.sent_lastfm_nowplaying)==='undefined'){
                                if(typeof(global.lastfm_session)==='undefined'){
                                    //console.log("Error: Last.fm session undefined");
                                    return;
                                }
                                lastfm.update('nowplaying', global.lastfm_session, {artist: artist, track: track, handlers:{ success: function(){
                                        showNotice("<strong>Last.fm</strong>: 'Now Playing':"+track+"("+artist+")","success");
                                    },error: function(error){
                                        showNotice("<strong>Last.fm</strong>: Could not update 'NowPlaying' stat for:"+track+"("+artist+")"+error.message,"error");
                                    }
                                }});
                                player.sent_lastfm_nowplaying = true;
                            }
                            if(typeof(player.sent_lastfm_scrobbled)==='undefined' 
                                && ((player.currentTime / player.duration) >= 0.4 ||  player.currentTime > (4*60*1000))) {
                                //Scrobble after 40% of the track was played or more than 4 minutes, whichever happens first.
                                console.log("Scrobbling:", (player.currentTime / player.duration));
                                console.log("Scrobbling:", player.currentTime);
                                if(typeof(global.lastfm_session)==='undefined'){
                                    //console.log("Error: the Last.fm session is undefined");
                                    return;
                                }
                                player.sent_lastfm_scrobbled = true;
                                lastfm.update('scrobble', global.lastfm_session, {artist: artist, track: track,timestamp: Math.floor(Date.now() / 1000), handlers:{
                                        success:function(){
                                            showNotice("<strong>Last.fm</strong>: Scrobbled <em>"+track+"("+artist+")</em>","success");
                                        },
                                        error:function(error){
                                            showNotice("<strong>Last.fm</strong>: Error while scrobbling <em>"+track+"("+artist+")+ Error:"+error.message+"</em>","success");
                                            console.log("Scrobbling Error",error);
                                        }

                                    }
                                });
                        
                        
                            }
                }
            });

            player.off('end');//Turn of earlier events for this.
            player.on('end', function() {
                console.log('Playing finished!');
                        //global.CURINDEX+=1;
                        //playTrackFromTorrent(global.TESTURN, global.CURINDEX);
                        //console.log('CurTrack:',global.playlist[global.PLAYLIST_INDEX,global.PLAYLIST_INDEX]);
                        if(global.playlist[global.PLAYLIST_INDEX]!==undefined) {
                            playNextTrack();
                        }else{
                            return;
                        }
                        //Already start registering next track in playlist.
                        //global.PLAYLIST_INDEX+=1;
                        /*if(global.playlist[global.PLAYLIST_INDEX]!==undefined) {
                            registerTorrent(global.playlist[global.PLAYLIST_INDEX]['urn'], global.playlist[global.PLAYLIST_INDEX]['file_indexnumber']);
                        }*/
                    });
        }
    }catch(e){
        console.log("Error while trying to play track:", e);
    }
}

global.knownsongs = {};
global.playlist = [];
global.query = '';
global.startpoint=0;
global.all_searchresults = [];

function searchTrack(query, startpoint, mode) {
    $('.back_to_results_link').fadeOut();
    $('#searchresults').unbind('scroll');
    $("#searchresults").attr("data-prevent-loadmore",false);
    global.query = query;

    if(typeof(startpoint) === 'undefined'){

        $('#searchresults').html($('.placeholder .loader').clone());
        global.startpoint = startpoint = 0;
        global.all_searchresults = [];
        $('.searchresults_title').html("Search Results &gt; <strong>"+query+"</strong>:");
        
    }else{
        global.startpoint = startpoint;
        $('#searchresults .loader').remove();
        $('#searchresults').append($('.placeholder .loader').clone());
    }

    if(mode !=='undefined' && mode == 'newest'){
        $('.searchresults_title').html("Recently Added to Server:");
        global.search_mode = 'newest';
        request(global.server_location+'/json.php?getNewest=1&startpoint='+startpoint, handle_searchresults);

    }else if((mode !=='undefined' && mode == 'favourite') || query==''){
        $('.searchresults_title').html("Most Popular Songs:");
        global.search_mode = 'favourite';
        request(global.server_location+'/json.php?mostFavourite=1&startpoint='+startpoint, handle_searchresults);
    
    }else{
        global.search_mode = 'search';
        request(global.server_location+'/json.php?qtrack='+query+'&startpoint='+startpoint, handle_searchresults);

    }
    
    global.searchresults_title = $('.searchresults_title').html();

}



function handle_searchresults(error, response, body) {
    //console.log(body);

    var indexie = 7;
    $('#searchresults .loader').remove();

    //Now, start loading the first torrent from this list.
    if(typeof response === 'undefined') {
        
        $('#searchresults').append("Could not connect to server.");
        return false;
    }else if(response.statusCode != 200) {
        $('#searchresults').append("Server returned an error. Is it on and properly configured?  " +response.statusCode);
        return false;
    }
    if(body === ''){
        $('#searchresults').append("Error: The current server seems to not understand this request. It might run outdated software.");
        return false;
    }
    try{
        //console.log(body);
        searchresults = JSON.parse(body);
    }catch(e) {
        $('#searchresults').append("Error:"+e);
        return false;
    }
    if(searchresults === undefined || searchresults.length == undefined || searchresults.length < 1) {
        if($('#searchresults .songcontainer').length < 1){
            $('#searchresults').html("No songs found (on this server).");
        }else{
            $('#searchresults').append("<strong>End of Results</strong>.");
        }
            return false;
    }else{
        showSearchResultTracks(searchresults);
        global.all_searchresults = global.all_searchresults.concat(searchresults);

        //Load more when scrolled down.
        function bindScroll(){
           if($('.searchwrapper').scrollTop() + $('.searchwrapper').innerHeight()  > $('#searchresults').height() - 100 && $("#searchresults").attr("data-prevent-loadmore") !="true") {
               $('.searchwrapper').unbind('scroll');
               searchTrack(global.query, global.startpoint+=1, global.search_mode);
           }
        }

        $('.searchwrapper').scroll(bindScroll);
    }


    //global.playlist = searchresults;
    $.each(searchresults, function(index,track) {
        if(typeof global.knownsongs[track['urn']] === 'undefined') {
            global.knownsongs[track['urn']] = {};
        }
        global.knownsongs[track['urn']][track['file_indexnumber']] = track;

    });
    //console.log(global.knownsongs);
    global.PLAYLIST_INDEX =0;

    //$('.metainfo').html(searchresults[global.PLAYLIST_INDEX]['trackname']+'--'+searchresults[global.PLAYLIST_INDEX]['artist']+'(album:'+searchresults[global.PLAYLIST_INDEX]['album_name']+')');
    /*
    global.TESTURN = searchresults[global.PLAYLIST_INDEX]['urn'];
    playTrack(searchresults[global.PLAYLIST_INDEX]);
    //global.CURINDEX = searchresults[global.PLAYLIST_INDEX]['file_indexnumber'];

    //global.PLAYLIST_INDEX+=1;
        if(global.playlist[global.PLAYLIST_INDEX+1]!==undefined) {
            registerTorrent(global.playlist[global.PLAYLIST_INDEX+1]['urn'], global.playlist[global.PLAYLIST_INDEX+1]['file_indexnumber']);
    }

    */


}

function showMostPopularSongs(){
    $('#searchresults').html('');
    searchTrack('', 0, 'favourite');
}
function showNewestSongs(){

    $("#searchresults").attr("data-prevent-loadmore",true);
    $('#searchresults').html('');
    searchTrack('', 0, 'newest');
}

function objsize(obj) {
    var size = 0;
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
}

function playTrack(trackinfo) {
    //Stop the music if it wasnt already stopped.
    if(typeof player !== 'undefined' && typeof player.playing  !== 'undefined' && player.playing) {
        player.stop();
        if(player.hasAlreadyPlayed){
            preloadTrack(player.trackinfo, player.track_index);//Reset track loading so it can be re-played on a new audio device.
        }
        
    }
    if(typeof trackinfo === 'undefined') {
        stopPlayerGracefully();
        //$('#playpause').html('<i class="glyphicon glyphicon-play"></i>');
        return false;
    }

    console.log("Playing Track:",trackinfo['trackname']);

    

    registerTorrent(trackinfo['urn'], trackinfo['file_indexnumber']);
    playTrackFromTorrent(trackinfo['urn'], trackinfo['file_indexnumber']);

    //$('.metainfo').html(trackinfo['trackname']+' — '+trackinfo['artist']+'(album:'+trackinfo['album_name']+')'+ "   Peers: "+objsize(global.torrents[trackinfo['urn']].swarm._peers));
    $('.songname','.metainfo').html("Song: "+trackinfo['trackname']);
    $('.artistname','.metainfo').html("Artist: "+trackinfo['artist']);
    $('.albumname','.metainfo').html("Album: "+trackinfo['albumname']);
    $('.stats','.metainfo').html("# Plays (on this server): "+ (trackinfo['plays'] !== undefined? trackinfo['plays']:'0 (Server does not know album)' ) );
    var albumUrl = grabAlbumCoverImageUrl($('#albumimage'),trackinfo);

    if(typeof(albumUrl) !== 'undefined'){
        $('.player_background_image').fadeOut('slow',function(){
            var gradient = 'linear-gradient(to bottom, rgba(0,0,0,0.9) 20%, rgba(0,0,0,0.1) 50%)';
            $('.player_background_image').css('background-image', gradient+',url('+albumUrl+')');
            $('.player_background_image').fadeIn('slow');
        });
    }

    // var notif = new DesktopNotification('Now Playing:'+trackinfo['album_name'], {body: $('.metainfo').html()});
    // notif.show();


    //Update state of play/pause button
    $('#playpause').html('<i class="glyphicon glyphicon-pause"></i>');

    //Change song that is 'selected' everywhere.
    $('.songcontainer, .playlistsong').removeClass('currentlyplaying');
    $('.songcontainer[data-urn="'+trackinfo['urn']+'"][data-file_indexnumber="'+trackinfo['file_indexnumber']+'"]').addClass('currentlyplaying');
    $('.playlistsong[data-playlist_index="'+global.PLAYLIST_INDEX+'"]').addClass('currentlyplaying');
}


//Only preloads a track without actually playing it.
function preloadTrack(trackinfo, track_index){
    //Play the sound from the port
        if(typeof global.playlist[track_index] === 'undefined'){
            return;
        }
        global.playlist[track_index].player = AV.Player.fromURL('http://localhost:'+global.music_server_portnum+'?urn='+trackinfo['urn']+'&fi='+trackinfo['file_indexnumber']);
        global.playlist[track_index].player.track_index = track_index; // Means that later, we can get the track index back if we only have a reference to the player object.
        global.playlist[track_index].player.trackinfo = trackinfo;
        //console.log("Preloading song:", global.playlist[track_index].player);

        $('.buffer_overlay', '.playlistsong[data-urn="'+trackinfo['urn']+'"][data-file_indexnumber="'+trackinfo['file_indexnumber']).css('width', 0 +'%');


        var curplayer = global.playlist[track_index].player;

        curplayer.on('buffer', function(percent){
            //console.log("Buffering Preloaded Track:"+track_index, percent);
            //console.log($('.buffer_overlay', '.playlistsong:eq('+track_index+')'));
            if(typeof(curplayer.scrollTimeout) != 'undefined'){
                clearTimeout(curplayer.scrollTimeout);
            }



            curplayer.scrollTimeout = setTimeout(function(){
                $('.buffer_overlay', '.playlistsong[data-urn="'+trackinfo['urn']+'"][data-file_indexnumber="'+trackinfo['file_indexnumber']).css('width', Math.max(100-percent, 0) +'%');
                $('.buffer_overlay', '.playlistsong[data-urn="'+trackinfo['urn']+'"][data-file_indexnumber="'+trackinfo['file_indexnumber']).css('opacity', (1-(percent/100)));
                $('.song_loader', '.playlistsong[data-urn="'+trackinfo['urn']+'"][data-file_indexnumber="'+trackinfo['file_indexnumber']).fadeOut();
            }, 250);
            
            
        });
        global.playlist[track_index].hasAlreadyPlayed = false;
        
        //$('.progressmoving').css('width',   0 +'%');


        global.playlist[track_index].player.preload();
        console.log('attempting to preload song...');

}


function playNextTrack() {
    $('#playpause').html('<i class="glyphicon glyphicon-play"></i>');
    if( global.playlist !== undefined) {
        if(global.shuffle_mode !== undefined && global.shuffle_mode === true){
            //Stop when whole playlist was done.
            var x = getShuffleTrackId();
            global.PLAYLIST_INDEX = x;
            if(x === global.start_shuffle_id){//If at end of looped playlist, auto stop.
                stopPlayerGracefully();
                return false;
            }
        
        }else if(global.playlist.length > global.PLAYLIST_INDEX){
            global.PLAYLIST_INDEX+=1;
        }else{
            stopPlayerGracefully();
            return false;
        }
        console.log("Playing Track:" + global.PLAYLIST_INDEX);
        playTrack(global.playlist[global.PLAYLIST_INDEX]);
        
    }else{
        stopPlayerGracefully();
        return false;
    }
}


function playPrevTrack() {
    $('#playpause').html('<i class="glyphicon glyphicon-play"></i>');
    if(global.playlist !== undefined) {
        if(global.shuffle_mode !== undefined && global.shuffle_mode === true){
            global.PLAYLIST_INDEX = getShuffleTrackIdBackwards();
        }else if(global.PLAYLIST_INDEX>0){
            global.PLAYLIST_INDEX -= 1;
        }else{
            return false;
        }
        playTrack(global.playlist[global.PLAYLIST_INDEX]);
    }
}
function playIfFirstFile() {
    if(typeof global.playlist !== 'undefined' && global.playlist.length == 1) {
        playTrack(global.playlist[global.PLAYLIST_INDEX]);
    }
}

function estimatePlayDuration() {
    //NOTE: VERY BAD ESTIMATION.
    var fileurn = global.playlist[global.PLAYLIST_INDEX - 1]['urn'];
    var fileindex = global.playlist[global.PLAYLIST_INDEX - 1]['file_indexnumber'];
    var filesize = global.torrents[fileurn].files[fileindex].length;
    var bytesPlayed = (player.device.device.amountOfRefills * player.device.device.bufferSize);
    var duration = ((filesize/bytesPlayed)* 15 /*stereo*/) * (player.currentTime);

    //console.log("Estimated duration:"+duration);
    return duration;
}

//setupMusicServer();
//searchTrack('vangelis');


//Testing function
function skipToEnd() {
    player.pause();
    player.seek(player.duration-5000);
    player.play();
}



function appendTrackToPlaylist(track) {
    global.playlist.push(track);
    var track_index = global.playlist.length -1;
    registerTorrent(track['urn'], track['file_indexnumber']);
    var trackelem = makePlaylistSongElement(track).hide();
    $('.playlistcontainer').append(trackelem);
    $(trackelem).hide().fadeIn(200);

    makeEachDraggable($(trackelem)[0]);
    $('.playlistcontainer').packery('reloadItems');
    $('.playlistcontainer').packery('layout');
    loadReorderedPlaylist();

    preloadTrack(track, track_index);
    playIfFirstFile();

}
/*
function addTrackToPlaylistAndPlay(track) {
    global.playlist.splice(global.PLAYLIST_INDEX, 0, track );
    registerTorrent(track['urn'], track['file_indexnumber']);
    $('.playlistcontainer .playlistsong:eq('+index+')').after(makePlaylistSongElement(track));
    playNextTrack();
}*/

function removeTrackFromPlaylist(index, dont_trigger_relayout) {
    //global.playlist.splice(index,1);
    /*$('.playlistcontainer .playlistsong[data-playlist_index="'+index+'"]').fadeOut(200, function(){
        $('.playlistcontainer').packery('remove', $('.playlistsong[data-playlist_index="'+index+'"]')[0]);
        if(typeof(dont_trigger_relayout)==='undefined'|| dont_trigger_relayout == false){
            $('.playlistcontainer').packery();
            loadReorderedPlaylist();
        }
    });*/

    $('.playlistcontainer .playlistsong[data-playlist_index="'+index+'"]').fadeOut(200,function(){
        $(this).remove();
        $('.playlistcontainer').packery('reloadItems');
        loadReorderedPlaylist();
        $('.playlistcontainer').packery('layout');

        if(global.PLAYLIST_INDEX >= global.playlist.length) {//If we removed the last song, stop.
            //player.stop();
            stopPlayerGracefully();
            global.PLAYLIST_INDEX = 0;
            return;
        }
        /*if(global.PLAYLIST_INDEX>index) { //If after this track in the list before removing, the index lowers by one.
            global.PLAYLIST_INDEX -= 1;
        }*/

        if(global.PLAYLIST_INDEX  == index && player.playing) {//If we were playing this track, go to the next track.
            playNextTrack(); 
        }
        //loadReorderedPlaylist();//Reset playlist layout to remake global.playlist order as well as DOM order.
        
    })



    
}


//Stops player if possible; Also resets GUI elems to their original state (empty title etc)
function stopPlayerGracefully(){
    try{
        $('.songname, .artistname, .albumname, .stats', '.metainfo').html('');
        $('.player_current_time, .player_duration').html('');
        $('#playpause').html('<i class="glyphicon glyphicon-play"></i>');

        $('.currentlyplaying').removeClass('currentlyplaying');
        player.stop();
        $('.progressmoving, .progressbuffered', '.progressbar').css('width', '0%');

    }catch(e){};
}



function makePlaylistSongElement(track) {
    var item = $('.placeholder .playlistsong').clone();
    $(item).attr('data-urn', track['urn']);
    $(item).attr('data-file_indexnumber', track['file_indexnumber']);
    $('.songname', item).html(longNameDots(track['trackname']));
    $('.songname', item).attr('title', track['trackname']);

    $('.artistname', item).html('By: '+track['artist']);
    $('.albumname', item).html(track['albumname']);
    $('.albumname', item).attr('title', 'Released on: '+track['albumname']);
    favdb.findSingleTrack(track['urn'],track['file_indexnumber'],function(err,data){
        $(item).addClass('favourited');
    })
    grabAlbumCoverImageUrl($('.albumimage', item),track);

    $('.removebtn', item).click(function(e) {
        console.log("Removing Button Clicked");
        removeTrackFromPlaylist($(this).parent().attr("data-playlist_index"));
        e.stopPropagation();
        e.preventDefault();
    });
    $('.favouritebtn',item).click(function(e){
        favdb.toggleFavourite(track,function(){
            $('[data-urn='+track['urn']+'][data-file_indexnumber='+track['file_indexnumber']+']').addClass('favourited');
        },function(){
            $('[data-urn='+track['urn']+'][data-file_indexnumber='+track['file_indexnumber']+']').removeClass('favourited');
        });
    });

    $(item).on("staticClick",function() {
        console.log();
        global.PLAYLIST_INDEX = +$(item).attr("data-playlist_index");//$(this).index();
        //console.log('The index: '+$(this).index());
        if(typeof global.playlist[global.PLAYLIST_INDEX] === 'undefined'){//Track was deleted.
            return;
        }
        playTrack(track);

    });


    var menu = new nw.Menu(); 

    // Add an item with label
    menu.append(new nw.MenuItem({ 
        label: 'Show Album',
        click: function(e) {
            //alert("WIP!");
            showSingleAlbum(track['urn'], track['albumname']);
        }
    }));
    menu.append(new nw.MenuItem({ 
        label: 'Copy Streaming URL',
        click: function(e) {
            //alert("WIP!");
            var url = 'http://127.0.0.1:'+global.music_server_portnum+'/?urn='+track['urn']+'&fi='+track['file_indexnumber']+(track['filename']=== undefined? '' : '&type=.'+getFileExtension(track['filename']));
            var clipboard = gui.Clipboard.get();
            clipboard.set(url, 'text');

            showNotice("The Streaming URL for <b>"+track['trackname']+"</b> was copied to your clipboard.<br/> You can now point an external app to this address (as long as the Cactus Player is open).","success",8000);
        }
    }));

    menu.append(new gui.MenuItem({ type: 'separator' }));

    menu.append(new nw.MenuItem({ 
        label: 'Artist Homepage',
        click: function(e) {
            gotoOfficialArtistHomepage(track['artist']);
        }
    }));

    menu.append(new nw.MenuItem({ 
        label: 'Last.fm Album Page',
        click: function(e) {
            gotoLastFmPage(track['albumname'], track['artist'])
        }
    }));

    $(item).on('contextmenu', function(e) {
        e.preventDefault();
        menu.popup(global.currentMousePos.x, global.currentMousePos.y);
    });


    return item
}

function longNameDots(name) {
    var maxlen = 15;
    if(name.length > maxlen) {
        return name.substr(0,maxlen-3)+'...';
    }else{
        return name;
    }
}

global.homepage_urls = {};
//Gives a '+1' to a loaded song, so the server knows it is available and also that it gets played. 
function rewardTrackLoaded(){
    if(typeof global.playlist[global.PLAYLIST_INDEX] === 'undefined'){
        return;
    }
    
    //Add song to 'support me' list
    if(global.playlist[global.PLAYLIST_INDEX]['counts_as_played'] === undefined){
        global.played_since_start+=1;
        global.global.played_since_last_support_modal+=1;
        global.playlist[global.PLAYLIST_INDEX]['counts_as_played'] = true;
        var artist = global.playlist[global.PLAYLIST_INDEX]['artist'];

        if(global.homepage_urls[artist] === undefined){
            global.artists_to_support.push(artist);
            global.homepage_urls[artist] = '';
            getOfficialArtistHomepage(artist, function(url){
                if(global.homepage_urls[artist] === undefined){
                    return; //was Already deleted;
                }
                global.homepage_urls[artist] = url;
            },function(){
                if(global.homepage_urls[artist] === undefined){
                    return; //was Already deleted;
                }
                global.homepage_urls[artist] = '';
            });
        }

        //global.artists_to_support = eliminateDuplicateArtists(global.artists_to_support);
        if( (global.played_since_last_support_modal > 4 )//Every 5 tracks
            ){
            global.played_since_last_support_modal = 0;
            //Timeout to a) Give last last URL the time to load., b) feel more fluid, don't crash GUI with too much stuff happening at the same time.
            setTimeout(function(){
                showSupportMeModal(eliminateDuplicates(global.artists_to_support));
            },10000);
            
        }
    }



    if(typeof(localStorage.CONFIG_reward_track_loaded)!== 'undefined' && localStorage.CONFIG_reward_track_loaded==false){
        return;//When explicitly turned off, do not send this signal.
    }
    try{
        
        if(typeof(global.playlist[global.PLAYLIST_INDEX]['rewardedTrack'])==='undefined'){
            request(global.server_location+'/json.php?rewardTrackLoaded='+global.playlist[global.PLAYLIST_INDEX]['urn']+'&file_index='+global.playlist[global.PLAYLIST_INDEX]['file_indexnumber'], function(error, response, body) {
                console.log("Track Rewarding finished. Status:", response, error, body);
            })
            global.playlist[global.PLAYLIST_INDEX]['rewardedTrack'] = true;
        }
    }catch(e){
        console.log("Track rewarding failed. Error:", e);
    }
}

var covers = {};//Assoc Array of cover image SRCs.
function grabAlbumCoverImageUrl(container, trackinfo) {
    if(typeof covers[trackinfo['urn']] !== 'undefined') {//If cached before, dont load again.
        var img = new Image();
        img.src = covers[trackinfo['urn']];
        $(container).html(img).hide().fadeIn();
        return img.src;
    }else{

        var albumname = trackinfo['albumname'];
        var artistname = trackinfo['artist'];
        var query = encodeURIComponent(albumname+' '+artistname);
        console.log("Query:"+ query);
        request('https://duckduckgo.com/i.js?q='+query+'%20music%20album%20cover&o=json', function(error, response, body) {
            //$('.albumimage').html(body);
            var img = new Image();
            img.onerror = function(){this.onerror=null;this.src=global.no_cover_img};
            //console.log(response, body);
            if(typeof response === 'undefined' || typeof response.statusCode === 'undefined' || response.statusCode != 200) {
                console.log('Error getting DDG response.');
                img.src=global.no_cover_img;
            }
            try{
                var data = JSON.parse(body);
            }catch(e) {
                console.log(e);
                img.src=global.no_cover_img;
            }

            //First, try again if maybe another track has already cached it before
             if(typeof covers[trackinfo['urn']] !== 'undefined') {//If cached before, dont load again.
                var img = new Image();
                img.src = covers[trackinfo['urn']];
                $(container).html(img);
                return img.src;
            }

            
            if(typeof data === 'undefined' || typeof data['results'] ==='undefined' || typeof data['results'][0] ==='undefined' || typeof data['results'][0].image/*.j*/ ==='undefined') {
                img.src = global.no_cover_img;
                covers[trackinfo['urn']] = global.no_cover_img; //No results, therefore insert no cover image in album list.
            }else{
                img.src = data['results'][0].image; // Originally: .j;
                covers[trackinfo['urn']] = data['results'][0].image; // Originally: .j;//Save for later use
            }
            $('.songcontainer[data-urn="' + trackinfo['urn'] + '"] .albumimage').html(img).hide().fadeIn();
            return img.src;
        });
    }
}
//grabAlbumCoverImageUrl($('#albumimage'),'chrono trigger', 'bla');



function showSearchResultTracks(searchresults) {
    console.log(searchresults);
    $.each(searchresults, function(index, track) {//Then load new tracks.
        createSearchResultsTrackElem(track,index);
    });
}

function createSearchResultsTrackElem(track,index){
        var item = $('.placeholder .songcontainer').clone();

        $(item).attr('data-urn', track['urn']);
        $(item).attr('data-file_indexnumber', track['file_indexnumber']);
        $('.songname', item).html(track['trackname']);
        $('.songname', item).attr('title', track['trackname']);
        $('.artistname', item).html('By: '+track['artist']);
        $('.albumname', item).html(track['albumname']);
        $('.artistname, .albumname', item).attr('title', 'Released on: '+track['albumname']);
        favdb.findSingleTrack(track['urn'],track['file_indexnumber'],function(err,data){
            $(item).addClass('favourited');
        });
        $('.favouritebtn',item).click(function(e){
            e.stopPropagation();
            favdb.toggleFavourite(track,function(){
                $('[data-urn='+track['urn']+'][data-file_indexnumber='+track['file_indexnumber']+']').addClass('favourited');
            },function(){
                $('[data-urn='+track['urn']+'][data-file_indexnumber='+track['file_indexnumber']+']').removeClass('favourited');
            });
        });
        $('.show_album_btn',item).click(function(e){
            e.stopPropagation();
            showSingleAlbum(track['urn'], track['albumname']);
        });


        grabAlbumCoverImageUrl($('.albumimage', item),track);

        //When clicked, load song in player.
        $(item).click(function() {
            //global.PLAYLIST_INDEX = index;
            //playTrack(track);
            appendTrackToPlaylist(track);

        });

        // Create an empty right click menu
        var menu = new nw.Menu(); 

        // Add an item with label
        menu.append(new nw.MenuItem({ 
            label: 'Append to Playlist',
            click: function(e) {
                appendTrackToPlaylist(track);
            }
        }));
        menu.append(new nw.MenuItem({ 
            label: 'Show Album',
            click: function(e) {
                showSingleAlbum(track['urn'], track['albumname']);
            }
        }));
        menu.append(new gui.MenuItem({ type: 'separator' }));

        menu.append(new nw.MenuItem({ 
            label: 'Artist Homepage',
            click: function(e) {
                gotoOfficialArtistHomepage(track['artist']);
            }
        }));

        menu.append(new nw.MenuItem({ 
            label: 'Last.fm Album Page',
            click: function(e) {
                gotoLastFmPage(track['albumname'], track['artist'])
            }
        }));













        $(item).on('contextmenu', function(e) {
            e.preventDefault();
            menu.popup(global.currentMousePos.x, global.currentMousePos.y);
        });

    $("#searchresults").append(item);    
}


function getFileFromPiece(torrent, piece){
   // var pieceLength = torrent.pieceLength;
   var curfile = {};//torrent.files[0];
    torrent.files.every(function(file, index, files){
        console.log('piece is: '+piece);

        if(piece * torrent.pieceLength < file.offset + file.length){
            curfile = file;
            curfile.index = index;
            return false;
        }else{
            console.log('Piece '+piece*torrent.pieceLength + ' was not in '+file.name + ' Because < ' + (file.offset + file.length) );
            return true;
        }
    });
    return curfile;
}

function makeEachDraggable(itemElem ) {
    // make element draggable with Draggabilly
    var draggie = new Draggabilly( itemElem, {handle: '.albumimage_box'} );
    // bind Draggabilly events to Packery
    $('.playlistcontainer').packery( 'bindDraggabillyEvents', draggie );
};

$(document).ready(function(){
    $('.playlistcontainer').packery({
        columnWidth: 110,
        rowHeight: 50,
        percentPosition: true,
        transitionDuration: "0.5s"
        

      });

    $('.playlistcontainer').packery( 'on', 'dragItemPositioned', function(){
        console.log("Reordering Playlist after dragging...");
        loadReorderedPlaylist();
    });
});

//Reload the playlist in its new order after the user dragged songs around.
function loadReorderedPlaylist(){
    var packery_list = $('.playlistcontainer').packery('getItemElements');
    console.log(packery_list);
    var new_list = [];
    try{
        $(packery_list).each(function(packery_index, item){
            var old_index = $(item).attr("data-playlist_index");
            if(typeof old_index === 'undefined'){
                old_index = $(item).index();
            }
            new_list[packery_index] = global.playlist[old_index];
            if(global.PLAYLIST_INDEX==old_index){
                global.PLAYLIST_INDEX=packery_index;
            }
            $(item).attr("data-playlist_index",packery_index);
        });
        //console.log(new_list);
        global.playlist = new_list;
        shuffleTracks();
        //console.log("New Playlist:",global.playlist);
    }catch(e){
        console.log(e);
    }
    $('.playlistcontainer').css("min-width", $('.playlistsong').length * 110 );//Restyle playlist container to fit more (or less ) songs
}




//Shows them in the search results.
function showSingleAlbum(urn, albumname){
    $('.searchresults_title').html("Album &gt; <strong>"+albumname+"</strong>:"+"<button id='add_whole_album' class='btn btn-default'>Add Album To Playlist</button>");
    $('#searchresults').html($('.placeholder .loader').clone());
    $("#searchresults").attr("data-prevent-loadmore",true);
    $('.back_to_results_link').fadeIn();
    

    console.log(global.server_location+'/json.php?album_urn='+urn);

    request(global.server_location+'/json.php?album_urn='+urn, function(error, response, body){
        $('#searchresults .loader').remove();
        console.log(error,response,body);
        if(response.statusCode != '200'){
            return;
        }
        try{
            var tracks = JSON.parse(body);
            console.log(tracks);
        }catch(e){
            console.log(e);
            return;
        }
        

        if(tracks.length < 1){
            $('#searchresults').html("The server does not know this album.");
        }else{
            showSearchResultTracks(tracks);
        }

        $("#add_whole_album").click(function(){
            appendAlbumToPlaylist(tracks);
        });
    });

    
}


function appendAlbumToPlaylist(albumlist){
    try{
        player.pause();
    }catch(e){};
    $.each(albumlist,function(index,track){
        global.playlist.push(track);
        registerTorrent(track['urn'], track['file_indexnumber']);
        var trackelem = makePlaylistSongElement(track)
        $('.playlistcontainer').append(trackelem);
        $(trackelem).css('opacity',0).fadeTo(400,1);
        makeEachDraggable($('.playlistcontainer .playlistsong:last')[0]);
        
        /*setTimeout(function(){
            appendTrackToPlaylist(track);
        },200)*/
    });
    //makeEachDraggable($('.playlistcontainer .playlistsong')[0]);
    $('.playlistcontainer').packery('reloadItems');//Load all new items
    loadReorderedPlaylist();
    $.each(global.playlist,function(index, track){
        preloadTrack(global.playlist[index], index);
    })
    setTimeout(function(){
       $('.playlistcontainer').packery('layout');//Reflow all items.
    },500)
    //playIfFirstFile();

    
    try{
        player.play();
    }catch(e){};
}

function showSupportMeModal(artists){
    $('ul.support_artists_list').html('');


    $.each(artists,function(index, artist){
        console.log('Parsing Artist:',artist);
        var item = $('.placeholder .supportme').clone();
        

        $('.artistname',item).html(artist);
        console.log($(item));
        $('ul.support_artists_list').append(item);

        if(global.homepage_urls[artist] !== ''){
            var url = global.homepage_urls[artist];
            console.log("I Found URL:",url);
            $('span.homepage:eq('+index+')','ul.support_artists_list').html(url);
            $('a.homepage_link:eq('+index+')','ul.support_artists_list').click(function(){
                gui.Shell.openExternal(url);
            });
            
        }else{
            $('span.homepage:eq('+index+')','ul.support_artists_list').html('');
            $('a.homepage_link:eq('+index+')','ul.support_artists_list').removeAttr('href');//change link styling.
        }
    });

    $("#support_artists_modal").modal('show');
    global.artists_to_support = [];//Reset.
    global.homepage_urls = {};//Reset all urls as well.
}

//Create a list of numbers in a different order but with the same length as the playlist.
global.shuffle_array = [];
global.shuffle_mode = false;
global.start_shuffle_id = 0;

function toggleShuffle(){
    if(global.shuffle_mode === false){
        global.start_shuffle_id = global.PLAYLIST_INDEX;
        shuffleTracks();
        global.shuffle_mode = true;
    }else{
        global.shuffle_mode = false;
    }
}

function shuffleTracks(){
    var len = global.playlist.length;
    if(len === 0){
        return;
    }
    var shufflearr = [];
    var seed_array = [global.start_time];

    for(var i = 0; i < len; i++){
        shufflearr[i] = i;
        seed_array.push(global.playlist[i]['file_indexnumber']);
    }
    //mt is seeded with a combination of the starting time of the program, and the current playlists' file indexes.
    //This means that whenever you reload the exact same file list during the same program opening, the shuffle order will be the same.
    //When you drag files around, or when you add/delete files, this number will of course change.
    var mt = Random.engines.mt19937().seedWithArray(seed_array);

    Random.shuffle(mt, shufflearr);

    global.shuffle_array  = shufflearr;

    //Rotate through array until the current song is at the start.
    if(   global.start_shuffle_id === undefined 
       || global.start_shuffle_id < 0 
       || global.start_shuffle_id > len){
        global.start_shuffle_id = 0; //In this case, set to 0 because otherwise we create an infinite loop.
    }
    var x = 0;
    do{
         x = getShuffleTrackId();
    } while(x !== global.start_shuffle_id);

    return global.shuffle_array;
}
//Shifts the array by one element, cyclical, and returns value of of old top.
function getShuffleTrackId(){
    var x;
    global.shuffle_array.push(x = global.shuffle_array.shift());
    return x;
}
//And the same for getting back to the other song.
function getShuffleTrackIdBackwards(){
    var x;
    global.shuffle_array.unshift(x = global.shuffle_array.pop());
    return x;
}