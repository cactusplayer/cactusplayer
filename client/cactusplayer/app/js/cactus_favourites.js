/*
The following information is stored:
-A primary ID
-Album Name
-Track Name
-Artist
-Genres

-file_indexnumber
-torrent URN
-torrent name
-file name

*/
var Datastore = require('nedb');
//var path = require('path');




$(document).ready(function(){
	favdb = new Datastore({ filename: path.join(require('nw.gui').App.dataPath, 'favourites.db'), autoload:true });
	
	//Define extra functions on singleton object.
	favdb.insertTrack = function(track_full){
		var track = serializeTrack(track_full);
		if(typeof track['urn'] ==='undefined' || typeof track['file_indexnumber'] === 'undefined'){
			console.log("Favdb Error: Important track info not set. Cannot insert track.");
			return;
		}
		favdb.insert(track,function(err,docs){
			console.log("favdb.insert",err,docs);
		});
	};

	favdb.removeTrack = function(track){
		if(typeof track['urn'] ==='undefined' || typeof track['file_indexnumber'] === 'undefined'){
			console.log("Favdb Error: Important track info not set. Cannot remove track.");
			return;
		}
		favdb.remove({urn: track['urn'], file_indexnumber: track['file_indexnumber']});
	};

	favdb.listFavourites = function(callback){
		if(typeof callback === 'undefined'){
			callback = function(){};
		}
		favdb.find({},function(err,docs){
			callback(err,docs);
		});
	};


	favdb.findSingleTrack = function(urn, file_indexnumber, callback,on_not_found){
		if(typeof callback === 'undefined'){
			callback = function(){};
		}
		if(typeof on_not_found === 'undefined'){
			on_not_found = function(){};
		}
		favdb.findOne({urn: urn, file_indexnumber: file_indexnumber},function(err,docs){
			if(docs!=null){
				callback(err,docs);
			}else{
				on_not_found(err,docs);
			}
		});
	};

	favdb.toggleFavourite = function(track_full,on_added,on_removed){
		if(typeof on_added === 'undefined'){
			on_added = function(){};
		}
		if(typeof on_removed === 'undefined'){
			on_removed = function(){};
		}


		var track = serializeTrack(track_full);
		favdb.findSingleTrack(track['urn'],track['file_indexnumber'],function(err,docs){
			favdb.removeTrack(track);
			on_removed();
		},function(){
			favdb.insertTrack(track);
			on_added();
		});
	};

	favdb.removeAll = function(callback){
		if(typeof callback === 'undefined'){
			callback = function(){};
		}
		favdb.remove({},{multi:true},callback);
	};

});


//Shows them in the search results.
function showFavouritesList(){
	if(favdb === undefined){//Wait until after page load.
		return;
	}
	 $('.searchresults_title').html(global.searchresults_title = "Your <strong>Favourites</strong>:");

	$('#searchresults').html($('.placeholder .loader').clone());
	$("#searchresults").attr("data-prevent-loadmore",true);
	favdb.listFavourites(function(err,tracks){
		global.all_searchresults = tracks;
		$('#searchresults .loader').remove();
		if(tracks.length < 1){
			$('#searchresults').html("You don't have any favourites yet.");
		}else{
			showSearchResultTracks(tracks);
		}
	});
}
