var nw = require('nw.gui');
var fs = require('fs'); //File System
var url = require('url');//URL-rewriting
var request = require('request');//Remote requests
var path = require('path');//local file system location checking (if files/folders exist)
var torrentStream = require('torrent-stream');//torrent-file streaming.

$(document).ready(function(){

	loadKnownHostsList();

	$('form#addAlbumForm').submit(function(e){
		e.preventDefault();
		handleSearchTorrentSubmission();
	});
	$('form#editAlbumForm').submit(function(e){
		e.preventDefault();
		$('.progress_info .loaderInfo').html('Saving to all servers...');
		saveToAllServers($('form#editAlbumForm').serialize());
	});

	$('a#guide_link').click(function(e){
		e.preventDefault();
		gui.Shell.openExternal($.trim(global.server_location + 'faq.php#add_album_guide'))
	})


	$('form#editAlbumForm').hide();
	$('form#editAlbumDiv').hide();
	$('.progress_info').hide();

	setTimeout(function(){
		var win = nw.Window.get();
		console.log(win);
		//win.resizeWindow(1024,786);
	},1);
});


function getUrn(urn_or_link){
	var regexp = /xt=urn:btih:([A-Ha-h0-9]+)&/;

}

function handleSearchTorrentSubmission(){
	var urn = $.trim($('input#urn').val());
	var torrentChecker = torrentStream(urn);
	global.musicFiles = [];
	console.log("URN:",urn,torrentChecker);
	$('.progress_info').html('Searching for Torrent Metadata...').fadeIn();
	$('form#editAlbumDiv').fadeOut();


	torrentChecker.on('ready',function(){
		torrentChecker.files.forEach(function(file, index) {
			var ext = getFileExtension(file.name);
			if(ext == 'mp3' || ext == 'wav' || ext == 'flac' || ext == 'aac'){ // Playable files
				global.musicFiles.push({index:index, path: file.name, name:getFilenameWithoutPath(file.name)});
		        console.log('filename:', file.name, cleanFilename(file.name, true));
			}
	    });
		showMetadataForm(torrentChecker, global.musicFiles);
		$('.progress_info').fadeOut();
	})
}

function showMetadataForm(torrentChecker, musicFiles){
	var torrentName = torrentChecker.torrent.name;
	var urn = torrentChecker.infoHash;
	console.log(torrentName, urn, musicFiles);
	var albumName = cleanFilename(torrentName, true);
	if(albumName.split('-').length == 2){//A very torrent name format is 'Artist - Album'
		var artist = $.trim(albumName.split('-')[0]);
		var albumName = $.trim(albumName.split('-')[1]);
	}else{
		var artist = '';
	}
	console.log(artist, albumName);


	$('form#editAlbumForm .urn').html(urn);
	$('form#editAlbumForm .torrentname').html(torrentName);
	$('form#editAlbumForm input#albumname').val(albumName);
	$('form#editAlbumForm input#artistname').val(artist);
	$('form#editAlbumForm input#genres').val('');

	$('form#editAlbumForm .fileinputs').html('');

	$.each(musicFiles,function(index,file){
		var item = $('.placeholder .single_track_input').clone();
		$('input.trackname',item).val(cleanFilename(file.name));
		$('input.trackname',item).attr('name','trackname_'+index);
		
		/* The following ones are hidden but important */
		$('input.filename_input',item).val(file.name);
		$('input.filename_input',item).attr('name','filename_'+index);

		$('input.fileindex_input',item).val(file.index);
		$('input.fileindex_input',item).attr('name','trackindex_'+index);

		$('.filename',item).html(file.path);
		$('form#editAlbumForm .fileinputs').append(item);
	});

	$('input#filecount','form#editAlbumForm').val(musicFiles.length);
	$('input#torrentname_input','form#editAlbumForm').val(torrentName);
	$('input#urn2','form#editAlbumForm').val(urn);

	$('form#editAlbumForm').fadeIn();

	


}

function saveToServer(url,data){

	$.post(url+'json.php?save_album_from_client=true', data, 
	function(result){
		console.log(result);
		try{
			var json = JSON.parse(result);
			if(typeof json.success !== undefined){

				showNotice(url+':<br/>'+json.success+'','success',0);
				$('form#editAlbumForm').fadeOut();
			}else{
				showNotice(url+':<br/>Error: '+json.error, 'danger', 0);
			}
		}catch(e){
			showNotice('Saving to <b>'+url+'</b> failed: <br/>'+e, 'warning', 0);
		}
		
	}).fail(function(){
		showNotice('Connecting to <b>'+url+'</b> was not possible.','warning', 0);
	});
}

function saveToAllServers(data){
	$.each(global.known_hosts.list, function(index,serverUrl){
		console.log('saving to server:'+serverUrl);
		saveToServer(serverUrl,data);
		//saveToServer('http://localhost/cactusplayer/server/',data);
	});
}


