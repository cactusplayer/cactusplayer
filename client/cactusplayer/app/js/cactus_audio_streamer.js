/*
    A lightweight Cactus Player Music Streamer.

    This is the Cactus Player, but now without the GUI or the player.
        This is the Music Streamer, that gets audio streams from torrents and passes them on to whomever listens to the correct port(s).
    It can be used to create a web player online.

*/




var fs = require('fs');
var http = require('http');
var url = require('url');//URL-rewriting
var torrentStream = require('torrent-stream');


var portnum = 3335;
global.music_server_portnum = portnum;
var playlist_filename = 'eponym_wm.cppl';

global.torrents = {};


var trackerslist = ["http://tracker.thepiratebay.org/announce",
    "udp://tracker.thepiratebay.org:80/announce",
    "http://tracker.publicbt.com/announce",
    "udp://tracker.publicbt.com:80/announce",
    "udp://tracker.openbittorrent.com:80/announce",
    "http://tracker.openbittorrent.com/announce",
    "http://tracker.deadfrog.us:42426/announce",
    "http://tracker.publicbt.com/announce",
    "udp://tracker.publicbt.com:80/announce",
    "udp://tracker.openbittorrent.com:80/announce",
    "http://tracker.openbittorrent.com/announce",
    "http://tracker.paradise-tracker.com:1...",
    "udp://tracker.openbittorrent.com:80",
    "http://tracker.torrenty.org:6969/anno...",
    "http://tracker.token.ro:80/announce",
    "http://tracker.publicbt.org:80/announce",
    "http://tracker.prq.to:80/announce",
    "http://tracker.publicbt.com:80/announce",
    "http://tracker.ydy.com:83/announce",
    "http://tracker.xpear.de:80/announce",
    "http://tracker.istole.it:80/announce",
    "http://tracker.ydy.com:97/announce",
    "http://tracker.harryy.us:80/announce",
    "http://photodiode.mine.nu:80/announce",
    "http://photodiode.mine.nu:6969/announce",
    "http://tracker.torrentparty.com:80/announce",
    "http://bt.peerseed.ru/announce",
    "http://tracker.ydy.com:103/announce",
    "http://tracker.openbittorrent.com:80/announce",
    "http://tracker.xpear.de:6969/announce",
    "http://denis.stalker.h3q.com:80/announce",
    "http://tracker.edgebooster.com:6969/announce",
    "http://pow7.com:80/announce",
    "http://tracker.publicbt.com:80/announce",
    "http://tracker.sladinki007.net:6500/a...",
    "http://tracker.xpear.de:6969/announce",
    "http://moviesb4time.biz/announce.php",
    "http://tracker.irc.su/announce",
    "http://tracker.prq.to/announce",
    "http://tracker.dunnsearch.org/announce.php",
    "http://tracker.irc.su:80/announce",
    "http://photodiode.mine.nu:6969/announce",
    "http://tracker.edgebooster.com:6969/announce",
    "http://tracker.bittorrent.am/announce",
    "http://denis.stalker.h3q.com:80/announce",
    "http://tpb.tracker.prq.to/announce",
    "http://tv.tracker.prq.to/announce",
    "http://tracker.bittorrent.am:80/announce",
    "http://tracker.prq.to/announce",
    "http://tpb.tracker.prq.to/announce",
    "http://www.ipmart-forum.com:2710/anno...",
    "http://tracker.ydy.com:105/announce",
    "http://www.torrentvideos.com:6969/ann...",
    "http://exodus.desync.com:6969/announce",
    "http://tracker.token.ro:80/announce",
    "http://tracker.publicbt.org:80/announce",
    "http://tracker.prq.to:80/announce",
    "http://tracker.publicbt.com:80/announce",
    "http://10.rarbg.com:80/announce",
    "http://tracker.xpear.de:80/announce",
    "http://tracker.istole.it:80/announce",
    "http://www.torrent-downloads.to:2710/...",
    "http://tracker.harryy.us:80/announce",
    "http://photodiode.mine.nu:80/announce",
    "http://photodiode.mine.nu:6969/announce",
    "http://tracker.torrentparty.com:80/announce",
    "http://bt.peerseed.ru/announce",
    "http://tracker.ydy.com:100/announce",
    "http://tracker.openbittorrent.com:80/announce"];



function registerFile(urn, fi) {
    if(fi!==undefined) {
        global.torrents[urn].files.forEach(function(file, index) {//Deselect all files, especially non-music files.
            //console.log('torrent filename:', file.name);
            //file.deselect();
            if(index==fi) {
                console.log("Attempting to download "+file.name+" from the BitTorrent network.");
                console.log("Play this file at: http://127.0.0.1:"+portnum+"/?urn="+urn+"&fi="+fi);
                file.select();
                //console.log('selecting file:'+ index +' -- '+file.name+' to play in the future!\n');
            }
        });
        
    }

}

function registerTorrent(urn, fi) {
    if (global.torrents[urn] !== undefined) {//If torrent already in cache, only add file.
        global.torrents[urn].on('ready', function() {
            registerFile(urn, fi);
        });
        return true;
    }
    global.torrents[urn] = torrentStream('magnet:?xt=urn:btih:'+urn, {trackers: trackerslist});//torrentStream('magnet:?xt=urn:btih:535194236610322444a7cf381ddf865e6be854fe&dn=Beatles+-+Hard+Day%26%23039%3Bs+Night+%28full+Album%29+320&xl=36214490&dl=36214490');
    global.torrents[urn].isready = false;
    global.torrents[urn].setMaxListeners(0);

    console.log('starting  connecting to torrent peers for '+urn);
    global.torrents[urn].on('ready', function() {
        console.log('finished connecting to torrent peers for '+urn);
        global.torrents[urn].isready = true;
        registerFile(urn, fi);
    });

    global.torrents[urn].on('download', function(pieceindex) {
        if(typeof readStream !=='undefined') {
            console.log('downloaded:'+ (pieceindex - readStream.startPiece ) + ' of ' + (readStream.endPiece - readStream.startPiece)+' ('+urn+') '  );
            /*var file = getFileFromPiece(global.torrents[urn].torrent, pieceindex);
            file.buffered = pieceindex * global.torrents[urn].torrent.pieceLength / file.size;
            console.log("Buffer Overlay:",buffer_overlay);
            console.log("Buffer Overlay File:",file.buffered);
            
            var buffer_overlay = $('.buffer_overlay', '.playlistsong[data-urn='+urn+']');
            buffer_overlay.css('width', (file.buffered*100) +'%');*/
        }
    });



    return global.torrents[urn];
}



function setupMusicServer() {


    console.log("Setting up music server at port "+portnum+"... ^C to quit.");
    //Load .mp3 in back-end
    http.createServer(function(request, response) {
        console.log("MusicServer is visited!");

        /* 1. Get file name */
        var url_parts = url.parse(request.url, true); 
        var GET_params = url_parts.query;
        if(GET_params.playlist_json === undefined && (GET_params.urn===undefined || GET_params.fi===undefined)) {
            response.writeHead(404);
            //response.write(JSON.stringify(GET_params));
            response.end();
            return false;
        }
        if(GET_params.playlist_json !== undefined){//return playlist json.
            response.setHeader("Access-Control-Allow-Origin","*");
            //response.writeHead(200);
            response.write(JSON.stringify(global.playlist));
            response.end();
            return true;
        }

        if(global.torrents[GET_params.urn] !== undefined && global.torrents[GET_params.urn].isready) {
            response.setHeader("Access-Control-Allow-Origin","*");
            returnMusicInfo(request, response, GET_params.urn, GET_params.fi);
        }else{
            global.torrents[GET_params.urn].on('ready',function() {returnMusicInfo(request, response, GET_params.urn, GET_params.fi)});
        }
        
    })
    .listen(portnum);
}

function forwardPlaylist(){
    return global.playlist;
}


function returnMusicInfo(request, response, urn, fi) {
    try{
        var finalfile = global.torrents[urn].files[fi];//Get the wanted file from engine.
    }catch(e){
        /* 2. If it does not exist, return with error. */
        console.log(e);
        return false;
    }




    /* 3. Otherwise, calculate content length and other headers */

    var fileLength = finalfile.length;
    var contentLen = fileLength;

    var httpheaders = {
        'Content-Type': 'audio/mpeg',
        'Accept-Ranges': 'bytes',
            'Content-Length': fileLength //stat.size
        };

        /* 4. Return content-range to enable streaming before whole file is downloaded.*/
        var bytStr = 'bytes=';
        if(request.headers.range && request.headers.range.substr(0,bytStr.length) == bytStr) {
            httpstatuscode = 206;
            console.log('range '+request.headers.range);
            var bytSelection = request.headers.range.substr(bytStr.length);

            var bytDashPos = bytSelection.indexOf('-');
            var bytPreDash = bytSelection.substr(0, bytDashPos);
            var bytEndDash = bytSelection.substr(bytDashPos+1);
            var offset = 0;

            if(bytPreDash == '0') {
                if(bytEndDash) {
                    var reqRangeLen = parseInt(bytEndDash,10);
                    if(reqRangeLen > contentLen) {
                        console.log('accept asking for invalid range');
                        rangeString = bytPreDash + '-' + (contentLen - 1);
                    } else {
                        contentLen = reqRangeLen+1;
                        rangeString = bytPreDash + '-' + bytEndDash;
                    }
                } else {
                    rangeString = '0-' + (fileLength-1).toString();//gs.length
                }
            } else if(bytEndDash != '' && bytPreDash != '') {
                contentLen = (parseInt(bytEndDash,10)+1) - parseInt(bytPreDash,10);
                offset = parseInt(bytPreDash,10);
                rangeString = bytPreDash + '-' + bytEndDash;
            } else if(bytEndDash == '' && bytPreDash != '') {
            // ex, 1234-
            contentLen = contentLen - parseInt(bytPreDash,10);
            offset = parseInt(bytPreDash,10) - 1;
            rangeString = bytPreDash + '-' + (fileLength - 1).toString();//gs.length
        }
        httpheaders["Content-Range"] = 'bytes ' + rangeString+'/'+fileLength;
        console.log('resulting content range:'+ httpheaders["Content-Range"]);
        httpheaders["Content-Length"] = contentLen;

    }else{
        // console.log('Visited without range request');
        httpstatuscode = 200;
        /*response.write("ASDF");
        response.end('');
        return false;*/
        
    }
    /* 4. If HEAD request, return file size as header field only.*/
    response.writeHead(httpstatuscode, httpheaders);
    if(request.method == 'HEAD') {
        console.log('doing a HEAD request');
        response.end('');
    }else{

        /* 5. Otherwise, read wanted part of file/torrent stream, and return to client.*/
        chunkSize = contentLen;

        if(chunkSize > fileLength - offset) {
            chunkSize = fileLength - offset;
        }

        readStream = finalfile.createReadStream({start: offset , end: offset + chunkSize });//fs.createReadStream(filePath);
        // We replaced all the event handlers with a simple call to readStream.pipe()
        console.log(finalfile.name);
        if(typeof readStream ===undefined) {
            response.writeHead(404);
            response.end();
            return false;
        }else{
            readStream.pipe(response);
        }

    }
}

function loadAndRegisterPlaylist(url){
  
    fs.readFile(url, {encoding: 'utf8'}, function(err, data) {
            if(err) {
               console.log("error"+err);
            }else{
                //console.log("File Contents:",data);
                try{
                    var playlist = JSON.parse(data);
                }catch(e){
                    console.log(e);
                    return;
                }
                if(playlist===undefined || playlist.tracks === undefined){
                    console.log("Could not read playlist format.");
                    return;
                }
                playlist.tracks.forEach(function(track,index,tracks){
                    registerTorrent(track.urn,track.file_indexnumber);
                });
                global.playlist = playlist;
            }
        });
}

setupMusicServer();

//registerTorrent('ab9a4ed2ec5284181c1f2687d47cf5524a2c34b5','1');
//registerTorrent('ab9a4ed2ec5284181c1f2687d47cf5524a2c34b5','2');
//registerTorrent('ab9a4ed2ec5284181c1f2687d47cf5524a2c34b5','3');

//loadAndRegisterPlaylist(playlist_filename);