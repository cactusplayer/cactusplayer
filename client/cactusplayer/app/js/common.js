/* Includes functions used by multiple pages. */


function showNotice(text, className, delayTime){
	if(typeof(className)==='undefined'){
		var className = 'info';
	}
	if($('p.alert:last-child').length>0){
		//var top = $('p.alert:last-child').css('top')+100;
	}else{
		//top='0';
	}
	var notice = $("<p class='alert text-center alert-"+className+"'>"+text+'<button type="button" class="close" data-dismiss="alert" ><span>&times;</span></button></p>');
	$(notice).hide();
	$('.alertarea').append(notice);
	var notice_inbody = $('p.alert:last-child');
	//$(notice_inbody).css('top', top+'px');
	
	if(delayTime==0){//if explicitly set to 0, keep notice indefinitely
		$('p.alert:last-child').fadeIn();
	}else{
		$('p.alert:last-child').fadeIn().delay(delayTime||2000).fadeOut(function(){ $(this).remove();} );
	}
	
}

function getFileExtension(filename){
	return filename.split('.').pop().toLowerCase();
}
function getFilenameWithoutPath(filename){
	return filename.split('/').pop();
}


function cleanFilename(filename, removeBrackets){
	if(typeof filename === undefined){
		filename = '';
	}
	filename = (''+filename).replace(/^([\s]*[\d]+)+[\s.-]*/gi, '');
	if(typeof removeBrackets !== undefined && removeBrackets == true){
		filename.replace(/((\[[\w\s\W]*\])|(\([\w\s\W]*\)))/gi, '');
	}
	filename = filename.replace(/torrent/gi, '');
	filename = filename.replace(/mp3/gi, '');
	filename = filename.replace(/flac/gi, '');
	filename = filename.replace(/reseed/gi, '');
	filename = filename.replace(/ music /gi, ' ');
	filename = filename.replace(/ ost /gi, ' ');
	filename = filename.replace(/CD/gi, '');
	filename = filename.replace(/320KB/gi, '');
	filename = filename.replace(/TBS/gi, '');
	filename = filename.replace(/_/gi, ' ');
	filename = filename.replace(/[.]/gi, ' ');

	return $.trim(filename);
}


global.known_hosts = {};
function loadKnownHostsList(){
    var containing_folder = path.dirname(process.execPath);
    fs.lstat(containing_folder+'/known_hosts.cphl', function(err, stats) {
    if (!err && stats.isFile()) {
        //File exists.
        fs.readFile(containing_folder+'/known_hosts.cphl', {encoding: 'utf8'}, function(err, data) {
            try{
                global.known_hosts = JSON.parse(data);
            }catch(e){
                alert("Error while trying to parse the known_hosts.cphl file:",e);
            }
            if(typeof global.known_hosts.current === 'undefined'){
                global.known_hosts.current = 0;
            }
            changeCurrentServer($.trim(global.known_hosts.current));
            if($('.known_hosts_list').length > 0){//Only do this when at the main player.
            	$('.known_hosts_list').html(showServersDomList());
            }
        });
    }else{
        //alert("Error: The known_hosts.cphl file which stores the list of known Cactus Player Hosts to connect to, is missing. Without it, I do not know what server to connect to.");
        //Set to empty to suppress errors.
        alert("Warning: The known_hosts.cphl file that is supposed to be in '"+containing_folder+"'' does not exists. A new one has been created, using a default connection to the main server.");
        global.known_hosts.list = ["https://cactusplayer.com/", "https://cactusplayer.com/test_server/"];
        global.known_hosts.current = 0;
        saveKnownHostsList();
        if($('.known_hosts_list').length > 0){//Only do this when at the main player.
        	$('.known_hosts_list').html(showServersDomList());
        }
    }
});
}


function saveKnownHostsList(){
    var data = JSON.stringify({list:global.known_hosts.list, current:global.known_hosts.current});
    var containing_folder = path.dirname(process.execPath);
    fs.writeFile(containing_folder+"/known_hosts.cphl", data, function(err) {
            if(err) {
               alert("error"+err);
            }
        });
}
function changeCurrentServer(index){
    index = Math.min(Math.max(index,0),global.known_hosts.list.length);
    global.known_hosts.current = index;
    global.server_location = $.trim(global.known_hosts.list[global.known_hosts.current]);
    console.log("Your will now use the server:",global.server_location);
    //updateKnownHosts();//Call update hosts on this new server right away.

    $('.known_hosts_list li').removeClass('current');
    $('.known_hosts_list li[data-host-index="'+global.known_hosts.current+'"]').addClass('current');
}

function eliminateDuplicates(arr) {
  var i,
      len=arr.length,
      out=[],
      obj={};

  for (i=0;i<len;i++) {
    obj[arr[i]]=0;
  }
  for (i in obj) {
    out.push(i);
  }
  return out;
}

function openExternal(url){
    gui.Shell.openExternal(url);
}