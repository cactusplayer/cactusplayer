# README #

The Cactus Player is an On-Demand Peer-to-Peer music player.
This repository contains both the source of the Cactus Player client as well as the server that one might run to support the network.

The Cactus Player is released under the GNU GPLv3 License, which means that you are free to download, adapt and share the source of this program, as long as it is released under the same license.

### What is this repository for? ###

This repository contains three important folders:
1. `client` contains the source code of the Cactus Player client. Right now it is configured to run as-is on Windows(x64). However, it should be possible to use a Mac or Linux version of node-webkit to run the application on those operating systems. Note that this is not yet tested.

2. (`bin_client` contains a packaged version of `client`).

3. `server` contains the code needed to set up your own Cactus Player Server Node. (Read more info below).

### What does the Cactus Player run on/with? ###
The Cactus Player client is built on **NW.js** (formerly node-webkit.js). It uses **Node.js** to maintain connections with the server(s) and the bitTorrent network (A huge thanks to **torrent-stream**). **Webkit** is used to display and handle the UI, combined with a variety of other web-based applications and plugins (See list below). **Aurora.js** is used to play the music files.

Other javascript resources used for the Cactus Player:

- jQuery
- Bootstrap
- IcoMoon
- FontSquirrel
- Tipped.js
- Packery.js

The Cactus Player Server on the other hand is built in PHP, and should also work on servers running slightly older PHP versions. The server only uses PHP, MySQL and Curl to gather, query and share the metadata.

### How to set up a server ###

Setting up a new server is designed to be easy.
Requirements:

- A webhost that runs PHP and MySQL.
- Using `curl` to access remote locations from PHP should be possible.

Setting Up:

- Copy all files from the `/server` folder to your test server. 
- Replace the known_hosts.cphl file with one from your own Client-Side player, which probably contains more hosts than the included one. 
- Edit the settings in config.php so the server can connect to your MySQL database and knows its location on the web.
- In PhpMyAdmin, run the included `sql/cactus_player_database_structure.sql` to set up the database structure. Delete the `sql` folder afterwards.



### Who do I talk to? ###

* If there are any problems, feel free to send an email to cactusplayer@gmx.com, or post issues in this repository.

Forks, Bug Reports and Bugfixes are greatly appreciated, as are feature requests.
Thank you for trying out this work-in-progress application.

### Roadmap ###
Working features:

Planned features:

 - Searching for/adding whole albums to playlist.
 - Nice detailed information screens on hover over album/track.
 - Possibility of having the client dissect the torrent file (when saving new torrent to server), removing the torcache.org dependency.
 - Shuffle function.

 - SPEED INCREASE.

 - Releasing packaged program versions for Windows, Mac, Linux.
 - Possibility of Google Chrome Plugin.
 - Sharing playlists by images (QR-Code?)