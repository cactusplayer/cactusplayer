<?php include "header.php"; ?>			
	<div class="content_wrapper">
			<h2>Frequently Asked Questions</h2>
		<div class="panel">
			<h3>What is Cactus Player?</h3>
			<p>Cactus Player is an application for Windows (A Linux and Macintosh version is in the works) that can play music.
				Other than other music-on-demand services out there, Cactus Player is <strong>decentralized</strong>, which means that there is no central regulating server.
			</p>
			<h3>In One Sentence:What is Cactus Player?</h3>
			<p>Cactus Player is Music-On-Demand meets Torrents. Cactus Player is Popcorn-Time for Music.</p>

			<h3>How does the Cactus Player work?</h3>
			<p>A very short summary:</a>
			<ol>
				<li>You search for the music you want to listen.</li>
				<li>A Server responds with a list of known songs/albums/artists.</li>
				<li>The Cactus Player downloads the songs you want to play through the Peer-to-Peer BitTorrent network, from listeners just like you.</li>
				<li>The Servers share their content with each other to keep the network stable.</li>
			</ol>
			<h3>There's music missing from the player that I like. What can I do?</h3>
			<p>
				If the music is already available as a Torrent, you can <a href="<?php echo $ROOT_TO_CURRENT; ?>parser/new_torrent_form.php">add it</a> to the decentralized server database.
			</p>
			<p>If it is not available as a torrent, <a href="http://torrentfreak.com/how-to-create-a-torrent/">it's very simple to create one</a>. 
			</p>
			<h3>What is a Cactus Player Server?</h3>
			<p>
				The Cactus Player Server is a simple webserver built in PHP and MySQL. It is analogous to a <strong>Phone Book:</strong> It keeps a list of known torrents(e.g. phone numbers) and what albums and tracks reside there (e.g. the phone owner's name).
				Because it is only a list, it is very lightweight.
				To keep the network as stable as possible, servers talk to eachother and keep their content lists more-or-less synchronized.
			</p>
			<h3>I cannot connect to the server. What can I do?</h3>
			<p>
				The Cactus Player keeps track of a list of known servers. Click on the cloud icon in the upper right corner of the application, and you can select another.
			</p>
			<h3>What audio formats can the Cactus Player play?</h3>

			<p>The Cactus Player uses <a href="https://github.com/audiocogs/aurora.js/" target="_blank">Aurora.js</a> to play its audio, which can handle .wav, .mp3 and .flac files. Other audio files cannot be played at this time.</p>

			<h3>I have a question that is not answered here.</h3>
			<p>You can take a look at the <a href="https://bitbucket.org/cactusplayer/cactusplayer/src" target="_blank">readme</a> of the Bitbucket Source Repository, which contains more information.</p>
			<p>Also, it is always possible to send an email to <script>var name='cactusplayer';var addr='gmx.com';document.write('<a href="mailto:'+name+'@'+addr+'">'+name+'@'+addr+'</a>');</script>.
		</p>
		</div>

		<h2 id="add_album_guide" style="color:white;"> Guide to Adding Albums</h2>
		<div class="panel">
		<p>Adding an album to the Cactus Player is supposed to be easy and accessible for everyone.
			To make the process as clear as possible, here is a short explanation:
		</p>
		<h3>How to make a new torrent</h3>
		<p>
			If you have music on your computer which you want to add, but it is not already available on the BitTorrent network, you'll have to do this step first.<br/>
			There are many guides available on the internet that explain how to create a torrent and start distributing it to the network. A good one is the <a href="http://torrentfreak.com/how-to-create-a-torrent/" target="_blank">Torrentfreak Guide</a>
		</p>
		<p>
			The Cactus Player plays files from selecting only that file from a torrent. Therefore, the Cactus Player can not handle torrents containing an archived (.zip, .tar, .gz etc) version of an album.
		</p>
		<p>
			The audio formats that the Cactus Player supports right now are .FLAC, .MP3, .WAV and .AAC . If your music is in a different format, you will need to convert it.
		</p>
		<h3>How to add a torrent to the Cactus Player System</h3>
			<p>If the music you want to add is available as a torrent, you click on the 'add new album' button in the player (or <a href="parser/new_torrent_form.php">Click Here</a>) to go to the <i>Add New Content</i> page of the server you are currently using.
			</p>
			<p>
			Here, you have to enter the Magnet Link of the torrent. Usually, this magnet link can easily be found within your torrent program. Otherwise, use a service such as <a href="http://torrent2magnet.com" target="_blank">Torrent2Magnet</a> to find it.
			When you click the 'Make Album' button, the system will read the file list from the torrent, and make educated guesses as to what the name of the album and the tracks on it are.
			</p> 
			<p>
				Of course, this might not always be perfect, so on the page that follows, you can check and edit the information to ensure that it is. You can then click on the 'save album' button to save it to the server.
			</p>
			<p>
				Congradulations! Now, the next time someone searches for this album, it will be shown in the search results in the Cactus Player.
			</p>
		<h3>I get an error, stating that the torrent could not be found</h3>
			<p>
				Right now, the server uses <a href="http://torcache.org" target="_blank">torcache.org</a> to gather the meta-information and file list from the torrent. This dependency will be removed in a future version of the Cactus Player.
			For now, the solution is to <a href="http://torcache.org" target="_blank">go to torcache.org</a> and upload your torrent to their cache.
			</p>
			<p>
				If the error persists, it might also be possible that you misspelled the magnet link, or that your torrent contains only unsupported files.
			</p>

			<br/><br/>
			<p>
			If you still have problems, feel free to send an email to <script>var name='cactusplayer';var addr='gmx.com';document.write('<a href="mailto:'+name+'@'+addr+'">'+name+'@'+addr+'</a>');</script>.
			</p>
		</div>
	</div>
<?php include "footer.php"; ?>