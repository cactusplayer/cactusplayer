<?php

	//MySQL Database Settings
	$SQL_HOSTNAME = 'localhost';
	$SQL_USER = 'root';
	$SQL_PASSWORD  = 'neverforget';
	$SQL_DATABASE = 'test';

	//The root URL of the server. IMPORTANT: Do not forget to add a trailing slash! Also, include the 'http://' part.
	$MY_URL = "http://localhost/cactusplayer/server/";
	//The name you fill in here is displayed in the footer of each page. This is of course not mandatory.
	$MAINTAINER_NAME = "mr. Cactus Himself";

	//File where other server locations are stored. You can just copy over a file from a client and store it in the same folder as this file.
	//Alternatively, point somewhere else, of course.
	$KNOWN_SERVERS_FILE = 'known_hosts.cphl';


	//You probably won't need to touch these.
	$SERVER_PING_CONNECT_TIMEOUT = 5;
	$SERVER_CONTENT_MERGE_CONNECT_TIMEOUT = 20;

	//For test servers: Set this flag to false to not gather content from other servers whenever someone queries this server.
	$DO_I_LOAD_SYNCHRONIZED_CONTENT = true;
	//For test servers: Set this flag to false to not broadcast its own URL to remote servers whenever asking for a server list.
	$DO_I_BROADCAST_MYSELF = false;

?>