			<footer>
				<p class="serverversion">This Cactus Player Server Runs: <strong><?php echo $CP_SERVER_VERSION ?></strong></p>
				<p class="maintainer">
				<?php
					if(isset($MAINTAINER_NAME) && $MAINTAINER_NAME != '')
					echo 'This Cactus Player Server is proudly maintained by:<strong>'.$MAINTAINER_NAME.'</strong>';
				?>
				</p>
				<p class="withlove">Made with ♥ for music.</p>
			</footer>
			
		</div>
	</body>
</html>