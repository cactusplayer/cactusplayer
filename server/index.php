<?php include "header.php"; ?>			
			<div class="hero-unit">
<h1>Instantly play your music.</h1>

			<p>		
				On-Demand Peer-To-Peer Music Player.
			</p>
			<button class="btn btn-default download_button">Download Now</button>
			</div>
			<div class="disclaimer">
				<img src="style/icon-warning.svg" class="warning-icon"/>
				<h2>Cactus Player streams music using the BitTorrent Protocol.</h2>
				<p>Dowloading and listening to copyrighted material may not be legal in your country. Use at your own risk.</p>
			</div>
			<div class="howitworks">
				<h1>It is this easy:</h1>
				<div class="triple">
					<div class="icon icon_search"></div>
					<p>Search for music you like and add it to your playlist.</p>
				</div>
				<div class="triple">
					<div class="icon icon_screenshot"></div>
					<p>The music automatically starts playing...</p>
				</div>
				<div class="triple">
					<div class="icon icon_network"></div>
					<p>...by being downloaded through the peer-to-peer torrent network!</p>
				</div>
			</div>
			<div class="download" id="download_section">
				<h1>Download a version of the Cactus Player</h1>

				<img src="style/icon-warning.svg" class="warning-icon"/>
					<b>Note: This is <em>Alpha-State</em> software: It probably contains bugs. Mileage may vary.</b>
				<?php include "downloads.php"; ?>

				<em class="slim_center_block" style="font-size:12px;">The Cactus Player is released under the <a href="https://www.gnu.org/licenses/gpl.html" target="_blank">GNU GPLv3</a>. This means that you are free to read, modify and re-distribute the source code, as long as your edited version is also released under the same open-source license.</em>
			</div>

			<script>
			$(document).ready(function(){
				$('.download_button').click(function(){
					var target = $('#download_section');
					var targetOffset = $(target).offset().top;
					$('html,body').animate({scrollTop: targetOffset}, 1000);
				});
				
			});

			</script>
<?php include "footer.php"; ?>			