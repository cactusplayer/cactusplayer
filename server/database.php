<?php /* This file contains the functions neccesary to talk with the database. */
require_once 'config.php';

$CP_SERVER_VERSION = 'v s0.8.7';

//Setup known servers.
$KNOWN_SERVERS = array();


function loadKnownServersFromFile(){
	global $KNOWN_SERVERS_FILE, $MY_URL, $KNOWN_SERVERS;

	$known_servers_data = file_get_contents($KNOWN_SERVERS_FILE,  FILE_SKIP_EMPTY_LINES);//Load servers file.
	if($known_servers_data != false){
		try{
			$known_servers = json_decode($known_servers_data, true);
			$KNOWN_SERVERS = $known_servers['list'];
			foreach($KNOWN_SERVERS as $index=>$node){
				if($node==$MY_URL || trim($node)==''){
				    unset($KNOWN_SERVERS[$index]);
				}else{
					$KNOWN_SERVERS[$index] = trim($node);
				}
			}
		}catch(Exception $e){
			die("Error while reading Known Servers File:".$e);
		}
	}else{
		die("Error: The Known Servers File, $KNOWN_SERVERS_FILE could not be found on the server.");
	}
}
loadKnownServersFromFile();

 

//Connect to database at load.
mysql_connect($SQL_HOSTNAME,$SQL_USER ,$SQL_PASSWORD);
@mysql_select_db($SQL_DATABASE) or die( "Unable to select database");


function q($query){
	$result = mysql_query($query);
	if (mysql_errno()) {
		$error = "MySQL error ".mysql_errno().": ".mysql_error()."\n<br>When executing:<br>\n$query\n<br>"; 
		//echo $error;
	}
	if(is_bool($result)){
		return $result;
	}else if(mysql_num_rows($result)>0){
		$answer = array();
		while ($row = mysql_fetch_assoc($result)) {
		    array_push($answer,$row);
		}
		return $answer;
	}else{
		return false;
	}
}

function insertNewPeerMusicAlbum($urn, $torrentname, $albumname, $artistname, $genres, $discogslink){
	$urn 			= strtolower(sanitize($urn));
	$torrentname 	= sanitize($torrentname);
	$albumname 		= sanitize($albumname);
	$artistname		= sanitize($artistname);
	$genres 		= sanitize($genres);
	$discogslink 	= sanitize($discogslink);


	$now = date('Y-m-d H:i:s');


	//$query = "UPDATE `p2pm_albums` SET (`torretname`='$torrentname',`albumname`='$albumname',`artist`='$artistname',`genres`='$genres',`inserted_at`='$now') WHERE `urn`='$urn'
	//	IF @@ROWCOUNT=0
    //		INSERT INTO `p2pm_albums` (`urn` ,`torrentname` ,`albumname` ,`genres` ,`artist` ,`discogs_id`, `inserted_at`) VALUES ('$urn', '$torrentname', '$albumname', '$genres', '$artistname', '$discogslink', '$now');";

	$query = "INSERT INTO `p2pm_albums` (`urn` ,`torrentname` ,`albumname` ,`genres` ,`artist` ,`discogs_id`, `inserted_at`) VALUES ('$urn', '$torrentname', '$albumname', '$genres', '$artistname', '$discogslink', '$now')
	ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), `torrentname`='$torrentname', `albumname`='$albumname', `artist`='$artistname', `genres`='$genres', `inserted_at`='$now'
	;";
	q($query);
}

//insertNewPeerMusicAlbum('test', 'bla', 'dd', 'asdf', 'dasdf', ', d', 'sadff', 4, 5);

function insertNewPeerMusicTrack($filename, $trackname, $file_indexnumber, $album_id){

	//$urn 			= sanitize($urn);
	$filename 		= sanitize($filename);
	$trackname 		= sanitize($trackname);
	//$artistname		= sanitize($artistname);
	//$genres 		= sanitize($genres);
	$file_indexnumber 	= sanitize($file_indexnumber);
	//$albumname 		= sanitize($albumname);
	//$seeds 			= sanitize($seeds);
	//$leechers 		= sanitize($leechers);
	$album_id = sanitize($album_id);


	//$query = "UPDATE `p2pm_tracks` SET (`filename`='$filename',`trackname`='$trackname') WHERE `album_id`='$album_id' AND `file_indexnumber`='$file_indexnumber'
	//	IF @@ROWCOUNT=0
    //		INSERT INTO `p2pm_tracks` (`filename`, `file_indexnumber`, `trackname`, `album_id`) VALUES ('$filename', '$file_indexnumber', '$trackname', '$album_id');";
	

	$remove_query = "DELETE FROM `p2pm_tracks` WHERE `file_indexnumber`='$file_indexnumber' AND `album_id`='$album_id';";
	q($remove_query);

	$query = "INSERT INTO `p2pm_tracks` (`filename`, `file_indexnumber`, `trackname`, `album_id`) VALUES ('$filename', '$file_indexnumber', '$trackname', '$album_id');";
	//print_r($query);
	q($query);
}
//insertNewPeerMusicTrack('test', 'bla', 'dd', 'asdf', 'dasdf', ', d', 'sadff', 5,6);


function cleanInput($input) {
 
  $search = array(
    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
  );
 
    $output = preg_replace($search, '', $input);
    return $output;
  }

function sanitize($input) {
    if (is_array($input)) {
        foreach($input as $var=>$val) {
            $output[$var] = sanitize($val);
        }
    }
    else {
        if (get_magic_quotes_gpc()) {
            $input = stripslashes($input);
        }
        $input  = cleanInput($input);
        $output = mysql_real_escape_string($input);
    }
    return $output;
}

function searchTrack($searchquestion, $startpoint){
	//$searchquestion = sanitize($searchquestion);
	$keywords = explode(',',trim($searchquestion));
	$searchquestion = sanitize($searchquestion);
	$startpoint = sanitize($startpoint);
	$resultsperpage = 30;
	$starting = $resultsperpage * $startpoint;

	//$query = "SELECT * FROM `p2pm_tracks` WHERE `artist` LIKE '%$searchquestion%' OR `genres` LIKE '%$searchquestion%' OR `trackname` LIKE '%$searchquestion%' OR `album_name` LIKE '%$searchquestion%'";
	
	//$query = "/*qc=on*/ SELECT * FROM `p2pm_tracks` t INNER JOIN `p2pm_albums` a WHERE t.id=a.id AND MATCH(artist,album_name,trackname,genres) AGAINST('$searchquestion' IN BOOLEAN MODE) ORDER BY MATCH(artist,album_name,trackname,genres) AGAINST('$searchquestion' IN BOOLEAN MODE) DESC, `plays` DESC  LIMIT $starting, $resultsperpage;";
	$query = "/*qc=on*/ SELECT * FROM `p2pm_tracks` t INNER JOIN `p2pm_albums` a WHERE t.album_id=a.id AND MATCH(a.artist,a.albumname,t.trackname,a.genres) AGAINST('$searchquestion' IN BOOLEAN MODE) ORDER BY MATCH(a.artist,a.albumname,t.trackname,a.genres) AGAINST('$searchquestion' IN BOOLEAN MODE) DESC, `plays` DESC, `inserted_at` DESC  LIMIT $starting, $resultsperpage;";
	//echo $query;
	$result = q($query);
	if(!isset($result)){

		$result = array();
	}
	
	return json_encode($result);
}

function searchAlbum($searchquestion, $startpoint){
	//$searchquestion = sanitize($searchquestion);
	$keywords = explode(',',trim($searchquestion));
	$searchquestion = sanitize($searchquestion);
	$startpoint = sanitize($startpoint);
	$resultsperpage = 1;
	$starting = $resultsperpage * $startpoint;

	//$query = "SELECT * FROM `p2pm_tracks` WHERE `artist` LIKE '%$searchquestion%' OR `genres` LIKE '%$searchquestion%' OR `trackname` LIKE '%$searchquestion%' OR `album_name` LIKE '%$searchquestion%'";
	
	//$query = "/*qc=on*/ SELECT * FROM `p2pm_albums` WHERE MATCH(artist,albumname,genres) AGAINST('$searchquestion' IN BOOLEAN MODE) AND EXISTS (SELECT NULL FROM `p2pm_tracks` WHERE `p2pm_tracks`.album_id = `p2pm_albums`.id) ORDER BY MATCH(artist,albumname,genres) AGAINST('$searchquestion' IN BOOLEAN MODE) DESC, `album_plays` DESC  LIMIT $starting, $resultsperpage;";
	$query = "/*qc=on*/ SELECT * FROM `p2pm_albums` WHERE MATCH(artist,albumname,genres) AGAINST('$searchquestion' IN BOOLEAN MODE) ORDER BY MATCH(artist,albumname,genres) AGAINST('$searchquestion' IN BOOLEAN MODE) DESC, `album_plays` DESC, `inserted_at` DESC   LIMIT $starting, $resultsperpage;";

	$result = q($query);

	if(!isset($result)){

		$result = array();
	}

	if(is_array($result)){

		foreach($result as &$album){
			$get_track_query = "SELECT * FROM `p2pm_tracks` WHERE `album_id`='". $album['id']. "';";
			$album['tracks'] = q($get_track_query);
			if(!isset($album['tracks'])){
				$album['tracks'] = array();
			}
		}
	}
	
	return json_encode($result);
}

//Returns a list of json_encoded tracks that are all part of the given URN/album.
function getAlbumTracksFromUrn($urn){
	$urn = sanitize($urn);
	$query1 = "SELECT * FROM `p2pm_albums` WHERE  urn='$urn' ORDER BY `id` ASC LIMIT 1;";
	$result = q($query1);
	if(isset($result) && is_array($result) && isset($result[0]) ){

		$album = $result[0];
		$query2 = "SELECT * FROM `p2pm_tracks` WHERE `album_id`='" . $album['id'] . "' ORDER BY `id` ASC";
		$result2 = q($query2);
		if(isset($result2) && is_array($result2)){
			$tracks = $result2;
			foreach($tracks as &$track){
				$track = $track + $album;
			}
			return json_encode($tracks);
		}else{
			return json_encode(array());
		}
	}else{
		return json_encode(array());
	}

}


//Increases the 'playable' count by one. Popular songs have this high. Is a score of availability.
function rewardTrackLoaded($torrent_urn, $fileindex){
	$torrent_urn = sanitize($torrent_urn);
	$fileindex = sanitize($fileindex);
	//$query = "UPDATE `p2pm_albums` SET `album_plays`= `album_plays` + 1 WHERE `urn`='$torrent_urn';";
	//$result = q($query);
	$query2 = "UPDATE `p2pm_tracks` t INNER JOIN `p2pm_albums` a SET `t`.`plays`= `t`.`plays` + 1, `a`.`album_plays`= `a`.`album_plays` + 1 WHERE `a`.`id`=`t`.`album_id` AND `a`.`urn`='$torrent_urn' AND `t`.`file_indexnumber`='$fileindex';";
	$result2 = q($query2);

	return json_encode(array($result2));
}

function loadMostFavouriteSongs($startpoint){
	$startpoint = sanitize($startpoint);
	if(!is_numeric($startpoint)){
		return json_encode(false);
	}
	$resultsperpage = 30;
	$starting = $resultsperpage * $startpoint;
	$query = "/*qc=on*/ SELECT * FROM `p2pm_tracks` t INNER JOIN `p2pm_albums` a WHERE t.album_id=a.id ORDER BY `plays` DESC LIMIT $starting, $resultsperpage;";
	$result = q($query);
	return json_encode($result);
}

function loadNewestSongs($startpoint){
	$startpoint = sanitize($startpoint);
	if(!is_numeric($startpoint)){
		return json_encode(false);
	}
	$resultsperpage = 30;
	$starting = $resultsperpage * $startpoint;
	$query = "/*qc=on*/ SELECT * FROM `p2pm_tracks` t INNER JOIN `p2pm_albums` a WHERE t.album_id=a.id ORDER BY `a`.`inserted_at` DESC, `plays` DESC LIMIT $starting, $resultsperpage;";
	$result = q($query);
	return json_encode($result);
}


function askAnotherServerForContent($query, $startpoint){
	global $KNOWN_SERVERS, $MY_URL, $SERVER_PING_CONNECT_TIMEOUT, $SERVER_CONTENT_MERGE_CONNECT_TIMEOUT, $DO_I_LOAD_SYNCHRONIZED_CONTENT;
	if(isset($DO_I_LOAD_SYNCHRONIZED_CONTENT) && $DO_I_LOAD_SYNCHRONIZED_CONTENT==false){//Flag for test servers. Don't sync them so other servers won't incorporate their results.
		return false;
	}
	//1. Pick another server from the list at random.
	$random_node = trim($KNOWN_SERVERS[array_rand($KNOWN_SERVERS)]);
	//var_dump($random_node);

	
	//2. Fire a searchAlbum query (e.g. the same one the user asked this server), Indicate with a flag that we are a server asking for content, to prevent recursion happening.
	$request = 'json.php?qalbum='.urlencode($query).'&startpoint='.$startpoint.'&server_request=true';
	//echo "Request:".$random_node.$request;

	// create curl resource
    $ch = curl_init();

    //Ask remote node for all servers it knows, to save for next time.
    //Only include my own URL if I want to tell other servers about myself (Which I don't want when I'm a test sever.)
    if(!isset($DO_I_BROADCAST_MYSELF) || $DO_I_BROADCAST_MYSELF = true){
    	curl_setopt($ch, CURLOPT_URL, $random_node . 'json.php?list_servers&my_server_address='.$MY_URL);
    }else{
    	curl_setopt($ch, CURLOPT_URL, $random_node . 'json.php?list_servers');
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $SERVER_PING_CONNECT_TIMEOUT);
    $remote_nodeslist = curl_exec($ch);
    addToServers(json_decode($remote_nodeslist, TRUE));

    // set url
    curl_setopt($ch, CURLOPT_URL, $random_node . $request);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $SERVER_CONTENT_MERGE_CONNECT_TIMEOUT);
    //echo "Request:". $random_node . $request;

    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);




    // $output contains the output string
    $remote_content = curl_exec($ch);
    //echo "Remote Content:";
    //var_dump($remote_content);


    // close curl resource to free up system resources
    curl_close($ch);

	//3. After re-sanitizing (Never trust your peers), insert all rows we found in the database.
	//var_dump($remote_content);
    handleAndSaveRemoteContent($remote_content);
}

function handleAndSaveRemoteContent($remote_content){

	try{
    	$remote_objects = json_decode($remote_content, TRUE);
    }catch(Error $e){
    	//Do nothing, just return.
   		return;
    }
    //var_dump($remote_objects[0]);
    if($remote_objects==NULL || !is_array($remote_objects) || count($remote_objects)<1){
    	//Request returned with an error.
    }else{
    	foreach($remote_objects as $key=>$album){


			$album_exists = q("SELECT `id` FROM `p2pm_albums` WHERE `urn`='".$album['urn']."' LIMIT 1;");
			$urn 			= sanitize($album['urn']);
			$torrentname		= sanitize($album['torrentname']);
			$discogslink		= sanitize($album['discogs_id']);
			$artistname		= sanitize($album['artist']);
			$genres 		= sanitize($album['genres']);
			$albumname 		= sanitize($album['albumname']);
			//$seeds 			= sanitize($album['seeds']);
			//$leechers 		= sanitize($album['leechers']);
			$album_plays 		= sanitize($album['album_plays']);
			$now = date('Y-m-d H:i:s');

			if($album_exists==false){//Only insert album to database if we didn't have one with that info already.
				q("INSERT INTO `p2pm_albums` (`urn` ,`torrentname` ,`albumname` ,`genres` ,`artist` ,`discogs_id`, `inserted_at`) VALUES ('$urn', '$torrentname', '$albumname', '$genres', '$artistname', '$discogslink', '$now');");
				$album_id = mysql_insert_id();
			}else{
				$album_id = $album['id'];
			}

			//echo "Album ID:".$album_id;
			
			//1. Get from database all rows that we already know for this album. 
			$known_tracks = q("SELECT `file_indexnumber` FROM `p2pm_tracks` WHERE `album_id`='$album_id'");
			if(!is_array($known_tracks)){
				$known_tracks = array();
			}else{
				foreach($known_tracks as &$track){
					$track = $track['file_indexnumber'];
				}
			}
			foreach($album['tracks'] as $key=>$track){
				//print_r($track);
	    		$filename 		= sanitize($track['filename']);
				$trackname 		= sanitize($track['trackname']);
				$file_indexnumber 	= sanitize($track['file_indexnumber']);
				$plays 		= sanitize($track['plays']);

				if(!in_array($file_indexnumber,$known_tracks)){//Skip songs we already have.
					insertNewPeerMusicTrack($filename, $trackname, $file_indexnumber, $album_id);
					$known_tracks[] = $file_indexnumber;
					
				}

				//array_push($sql_strings, "('$urn', '$filename', '$file_indexnumber', '$trackname', '$artistname', '$genres', '$albumname', '$seeds', '$leechers', '$plays')");
	    	}
			//Inserts all tracks, unless duplicates exist. 
		   	
		    
	   	}


    }
	//(4. After this, do the query as normal). If no connection could be made with server, also do query as normal.
}

function listServers(){
	global $KNOWN_SERVERS, $MY_URL;
	if(isset($_GET['my_server_address'])){
		$new_server = trim($_GET['my_server_address']);
		if(filter_var($new_server,FILTER_VALIDATE_URL)){
			addToServers(array($new_server));
		}
	}
	return json_encode(array_merge($KNOWN_SERVERS, array($MY_URL)));
}

function addToServers($new_nodes){
	global $KNOWN_SERVERS, $KNOWN_SERVERS_FILE, $MY_URL;
	if(!is_array($new_nodes)){
		return;
	}

	$total_nodes = array_merge($KNOWN_SERVERS, $new_nodes);
	foreach($total_nodes as $index=>$node){
		if($node==$MY_URL || trim($node)=='' || !filter_var($node, FILTER_VALIDATE_URL)){
		    unset($total_nodes[$index]);
		}else{
			$total_nodes[$index] = trim($node);
		}
	}
	$total_nodes = array_unique($total_nodes);
	$total_nodes = array_values(array_filter($total_nodes));
	$KNOWN_SERVERS = $total_nodes;
	file_put_contents($KNOWN_SERVERS_FILE, json_encode(array('list'=>$KNOWN_SERVERS,'current'=>0)));

}

?>