<?php
if(!isset( $ROOT_TO_CURRENT)){
 $ROOT_TO_CURRENT = '';
 require_once('database.php');
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Cactus Player: Decentralized Peer-To-Peer Music Player</title>


		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" href="favicon.ico" /> 

		<meta charset="utf-8">  
		<script src="<?php echo $ROOT_TO_CURRENT; ?>style/js/jquery.min.js"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo $ROOT_TO_CURRENT; ?>style/normalize.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $ROOT_TO_CURRENT; ?>style/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $ROOT_TO_CURRENT; ?>style/bootstrap-theme.min.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $ROOT_TO_CURRENT; ?>style/style.css" />
	</head>
<body>
		<div class="pagewrapper">
			<header>

				<a href="<?php echo $ROOT_TO_CURRENT; ?>index.php"><div class="title"><div class="logo"></div><h1>Cactus Player</h1> </div></a>
				<nav class="menu pull-left">
					<ul>
						<li><a href="<?php echo $ROOT_TO_CURRENT; ?>parser/new_torrent_form.php">Add Music!</a></li>
					</ul>
				</nav>
				<nav class="menu">
					<ul>
						<li><a href="<?php echo $ROOT_TO_CURRENT; ?>faq.php">Frequently Asked</a></li>
					</ul>
				</nav>
				<div class="socialmedia"></div>
			</header>
			<div class="currentversion_notice">
				
			</div>