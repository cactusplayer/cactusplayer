<?php
//require_once '../config.php' ;
//require_once '../database.php';

require_once 'class.bdecode.php';


function getTorrentFile($URN){
	$url = "http://torcache.net/torrent/".$URN.".torrent";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$tempfilename = 'temp/'.$URN.'_temp.torrent';
	$tempfile = fopen($tempfilename, 'w');
	curl_setopt($ch, CURLOPT_FILE, $tempfile);
	curl_setopt($ch, CURLOPT_ENCODING, ''); //Remove gzip
	curl_setopt($ch, CURLOPT_TIMEOUT, 60);//Max of 1 minute per torrent waiting time to download file. This because sometimes the files are rather large.
	curl_exec($ch);
	curl_close($ch);
	$torrentdata = parseTorrentInfo($tempfilename);
	fclose($tempfile);
	unlink($tempfilename);//Remove the temporary torrent file.
	return $torrentdata;
}


//TODO: Load track names/lengths from album resource..
function getDiscogsAlbumSearch($query){ 

	//TODO: Move these?
	$APP_KEY = "qekEKxHhTJuFYKvmTkUq";
	$APP_SECRET = "PPpyVtxrMviaFEucIZBcxpuehucLCRza";



	$query = urlencode($query);

	//Type is 'release', because we dont want artists mixed in our results.
	//per_page is 1, because we only need the first result.
	$search_url = "https://api.discogs.com/database/search?q=$query&key=$APP_KEY&secret=$APP_SECRET&type=release&per_page=1&page=1";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $search_url);
	curl_setopt($ch, CURLOPT_USERAGENT, 'PeerMusicParser/0.1');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
	
	$curl_result = curl_exec($ch);
	

	$result = json_decode($curl_result, true);


	//Now, load resource URL
	if(isset($result['results'][0])){
		echo "Resource URL: ".$result['results'][0]['resource_url']."\n\n";
		curl_close($ch);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $result['results'][0]['resource_url']);
		curl_setopt($ch,CURLOPT_USERAGENT, 'PeerMusicParser/0.1');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 

		$curl_result2 = curl_exec($ch);
		$resource = json_decode($curl_result2, true);
	}else{
		curl_close($ch);
		return false;
	}

	

	if(isset($resource)){ //Return the first album result.
		//print_r($resource);
		curl_close($ch);
		return $resource;
	}else{
		curl_close($ch);
		return false;
	}
	
}

function parseTorrentInfo($filename){

	$torrent = new BDECODE($filename);

	return $torrent->result;
}


function parseSingleTorrent($torrent){

			$torrentinfo = getTorrentFile($torrent[2]);
			$torrentInternalFiles = $torrentinfo['info']['files'];


			

			$is_musical = false;//Becomes true when there is a file that is a music file.
			$musicalfiles =  array();

			if(!isset($torrentInternalFiles) || count($torrentInternalFiles)<1){
				//Single-file torrent. Cannot be parsed to album/tracks format right now. Maybe in the future?
				return false;
			}
			foreach($torrentInternalFiles as $index => $file){


				$extension = pathinfo($file['path'][count($file['path'])-1], PATHINFO_EXTENSION );
				if($extension == 'mp3' || $extension == 'wav' || $extension == 'ogg' || $extension == 'flac' || $extension == 'aac'){
					$filename = pathinfo($file['path'][count($file['path'])-1], PATHINFO_BASENAME );
					
					//print_r($file);

					$is_musical = true;
					$fullfile = $file;
					$fullfile['index'] = $index;
					array_push($musicalfiles,$fullfile);
				}

			}

			if($is_musical){


				$clean_album_name = cleanFilename($torrentinfo['info']['name']);

				echo "Clean Album Name: $clean_album_name \n\n";

				$discogs_album = getDiscogsAlbumSearch($clean_album_name);

				if($discogs_album===false){
					echo "Error: No album found for this torrent!\n\n";
				}else{
					//Now, save Album to database.
					$urn = $torrent[2];

					$torrentname 	= $torrentinfo['info']['name'];
					$albumname 		= $discogs_album['title'];
					$artistname		= $discogs_album['artists'][0]['name']; //TODO: Support for multiple artists?
					$genres 		= implode($discogs_album['genres'], ', ');
					$discogslink 	= $discogs_album['resource_url'];

					$seeds = $torrent[5];
					$leechers = $torrent[6];

					insertNewPeerMusicAlbum($urn, $torrentname, $albumname, $artistname, $genres, $discogslink, $seeds, $leechers);

					foreach($musicalfiles as $track){

						$filename = pathinfo($track['path'][count($track['path'])-1], PATHINFO_BASENAME );
						
						$file_indexnumber = $track['index'];
						$trackname = 'TODO';//TODO
						if(isset($discogs_album['tracklist'][$file_indexnumber])){
							$trackname = $discogs_album['tracklist'][$file_indexnumber]['title'];
						}else{
							$trackname =  pathinfo($track['path'][count($track['path'])-1], PATHINFO_FILENAME );//If not exists, use filename without extension instead.
							$trackname = str_ireplace('_', ' ', $trackname);
							$trackname = str_ireplace('.', ' ', $trackname);
							
						}

						echo "File index: $file_indexnumber  --> $filename | $trackname \n";

						echo "insertNewPeerMusicTrack($urn, $filename, $trackname, $artistname, $genres, $file_indexnumber, $albumname, $seeds, $leechers)";


						insertNewPeerMusicTrack($urn, $filename, $trackname, $artistname, $genres, $file_indexnumber, $albumname, $seeds, $leechers);
					}
				}

			}
			
}

function saveAlbumFromClient(){
	$urn = trim($_POST['urn']);
	$torrentname = trim($_POST['torrentname']);
	$albumname = trim($_POST['albumname']);
	$artistname = trim($_POST['artistname']);
	$genres = trim($_POST['genres']);
	$filecount = trim($_POST['filecount']);
	if(!is_numeric($filecount) || $filecount == 0){
		return json_encode(array('error'=>"This torrent contains no playable files and thus was not added."));
	}

	for($index=0;$index<$filecount;$index++){//Ensure that all files actually exist in the post data instead of saving a malformed album.
		if(	   !isset($_POST['filename_'.$index])
			|| !isset($_POST['trackname_'.$index])
			|| !isset($_POST['trackindex_'.$index]) || !is_numeric($_POST['trackindex_'.$index])
			){
			return json_encode(array('error'=>"Error during form submission."));
		}
	}

	insertNewPeerMusicAlbum($urn, $torrentname, $albumname, $artistname, $genres, '');
	$album_id = mysql_insert_id();
	//echo "<p class='error'>Note: Album ID is: <strong>$album_id</strong></p>";

	for($index=0;$index<$filecount;$index++){
		$filename = $_POST['filename_'.$index];
		$trackname = trim($_POST['trackname_'.$index]);
		$file_indexnumber = $_POST['trackindex_'.$index];

		insertNewPeerMusicTrack($filename, $trackname, $file_indexnumber, $album_id);

		//echo "<p class='success'>Song: <strong>". $trackname. "</strong> was successfully entered into the database.</p>";
	}
	return json_encode(array('success'=>"Album <b>".$albumname."</b> saved successfully"));
}


function cleanFilename($filename, $removeBrackets = true){
	
	$filename = preg_replace('/^([\s]*[\d]+)+[\s.-]*/', '', $filename);
	if($removeBrackets){
		$filename = preg_replace('/((\[[\w\s\W]*\])|(\([\w\s\W]*\)))/', '', $filename);
	}
	$filename = str_ireplace('torrent', '', $filename);
	$filename = str_ireplace('mp3', '', $filename);
	$filename = str_ireplace('flac', '', $filename);
	$filename = str_ireplace('reseed', '', $filename);
	$filename = str_ireplace('music', '', $filename);
	$filename = str_ireplace('ost', '', $filename);
	$filename = str_ireplace('CD', '', $filename);
	$filename = str_ireplace('320KB', '', $filename);
	$filename = str_ireplace('TBS', '', $filename);
	$filename = str_ireplace('_', ' ', $filename);
	$filename = str_ireplace('.', ' ', $filename);

	return trim($filename);
}

?>