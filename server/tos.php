<?php include "header.php"; ?>			
	<div class="content_wrapper">
		<div class="panel">
			<strong style="color:red;">
				<p>By using Cactus Player, you agree that you are aware of the Copyright/Digital Content Sharing laws in your country, and abide with them. </p>

				<p>The creators and maintainers of the Cactus Player do not have complete control over the music that exists on the network, and cannot be held accountable.</p>
			</strong>
		</div>
	</div>
<?php include "footer.php"; ?>			