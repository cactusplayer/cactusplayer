<?php

/* Is included by index.php . Contains a list/table of sources to download the different versions of the Cactus Player.
 * When new versions of the Cactus Player are released, only updating this file should be enough, (as long as no server features change.)
 */

$program_versions = array("0-5-0", "0-4-0", "0-3-0b", "0-2-0b");
$magnet_links = array(	"magnet:?xt=urn:btih:CABA1283B5FF78016D544CFBFF5307B1CD5ADDE4&dn=cactusplayer_v0-5-0_zipped&tr=udp%3a%2f%2ftracker.openbittorrent.com%3a80%2fannounce&tr=udp%3a%2f%2ftracker.publicbt.com%3a80%2fannounce&tr=udp%3a%2f%2ftracker.ccc.de%3a80%2fannounce&tr=http%3a%2f%2fopen.tracker.thepiratebay.org%2fannounce&tr=http%3a%2f%2fwww.torrent-downloads.to%3a2710%2fannounce&tr=http%3a%2f%2fdenis.stalker.h3q.com%3a6969%2fannounce&tr=udp%3a%2f%2fdenis.stalker.h3q.com%3a6969%2fannounce&tr=http%3a%2f%2fwww.sumotracker.com%2fannounce",
						"magnet:?xt=urn:btih:FE07F6E30DD013E4A5D397F1B27AFC75E47D1F00&dn=cactusplayer_v0-4-0_zipped&tr=udp%3a%2f%2ftracker.openbittorrent.com%3a80%2fannounce&tr=udp%3a%2f%2ftracker.publicbt.com%3a80%2fannounce&tr=udp%3a%2f%2ftracker.ccc.de%3a80%2fannounce&tr=http%3a%2f%2fopen.tracker.thepiratebay.org%2fannounce&tr=http%3a%2f%2fwww.torrent-downloads.to%3a2710%2fannounce&tr=http%3a%2f%2fdenis.stalker.h3q.com%3a6969%2fannounce&tr=udp%3a%2f%2fdenis.stalker.h3q.com%3a6969%2fannounce&tr=http%3a%2f%2fwww.sumotracker.com%2fannounce",
						"magnet:?xt=urn:btih:5D2D33AF608A28697878E19C929A74B97478725D&dn=cactusplayer_v0-3-0_zipped&tr=udp%3a%2f%2ftracker.openbittorrent.com%3a80%2fannounce&tr=udp%3a%2f%2ftracker.publicbt.com%3a80%2fannounce&tr=udp%3a%2f%2ftracker.ccc.de%3a80%2fannounce&tr=http%3a%2f%2fopen.tracker.thepiratebay.org%2fannounce&tr=http%3a%2f%2fwww.torrent-downloads.to%3a2710%2fannounce&tr=http%3a%2f%2fdenis.stalker.h3q.com%3a6969%2fannounce&tr=udp%3a%2f%2fdenis.stalker.h3q.com%3a6969%2fannounce&tr=http%3a%2f%2fwww.sumotracker.com%2fannounce");
?>

<table class="programversions_table">
<thead><tr><th>Version</th><th><i class="large_icon icon-windows"></i>Windows</th><th><i class="large_icon icon-apple"></i>Macintosh</th><th><i class="large_icon icon-linux"></i>Linux</th></tr></thead>
<tbody>

	<?php foreach($program_versions as $index=>$version){ ?>
	<tr>
		<td>Version <?php echo str_replace('-', '.', $version); ?></td>
		<td><a class="program_version" href="http://www.cactusplayer.com/downloads/v<?php echo $version ?>/cactusplayer_v<?php echo $version ?>_windows-x32.zip" 	target="_blank" >x32</a> <a class="program_version" href="http://www.cactusplayer.com/downloads/v<?php echo $version ?>/cactusplayer_v<?php echo $version ?>_windows-x64.zip" target="_blank" >x64</a></td>
		<td><a class="program_version" href="http://www.cactusplayer.com/downloads/v<?php echo $version ?>/cactusplayer_v<?php echo $version ?>_mac-x32.zip" 		target="_blank" >x32</a> <a class="program_version" href="http://www.cactusplayer.com/downloads/v<?php echo $version ?>/cactusplayer_v<?php echo $version ?>_mac-x64.zip" target="_blank" >x64</a></td>
		<td><a class="program_version" href="http://www.cactusplayer.com/downloads/v<?php echo $version ?>/cactusplayer_v<?php echo $version ?>_linux-x32.zip" 		target="_blank" >x32</a> <a class="program_version" href="http://www.cactusplayer.com/downloads/v<?php echo $version ?>/cactusplayer_v<?php echo $version ?>_linux-x64.zip" target="_blank" >x64</a></td>
		<td><?php if(isset($magnet_links[$index]) && $magnet_links[$index] != '' ){ ?>
			<a class="program_version torrent_link" href="<?php echo $magnet_links[$index]; ?>">.torrent</a></td>
			<?php } ?>
	</tr>

	<?php } ?>
</tbody>
</table>
<!--
<h3>Mirrors:</h3>
<a class="program_version" href="https://bitbucket.org/cactusplayer/cactusplayer/downloads" target="_blank" >Bitbucket</a>
-->
<p>Alternatively, you can try building a version from source:</p>

<a href="https://bitbucket.org/cactusplayer/cactusplayer" target="_blank" ><strong>BitBucket Source Repository</strong></a>